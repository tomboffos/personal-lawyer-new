abstract class AuthorizationEvent{}

class LoginEvent extends AuthorizationEvent{}

class RegisterEvent extends AuthorizationEvent{}