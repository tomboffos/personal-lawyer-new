const Map<String,dynamic> ruRu = {
  "personal-lawyer": "Ваш личный адвокат",
  "get-consultation" : "Получить консультацию",
  "document-template" : "Шаблоны документов",
  "technical-support" : "Обратиться в тех поддержку",
  "main" : "Главная",
  "order" : "Заказ",
  "question" :"Вопрос",
  "messages" : "Сообщение",
  "documents" : "Мои документы",
  "settings" : "Мои настройки",
  "personal-data" : "Личные данные",
  "change-pincode": "Сменить код доступа",
  "cards" : "Мои карты",
  "exit" : "Выход",
  "your-question" : "Ваш вопрос",
  "lawyer" : "Юрист",
  "connect" : "С кем вы хотите связаться?",
  "interest" : "Что вы хотите узнать?",
  "pay" : "Оплатить",
  "chat-closed" : "Ваш чат закрыт",
  "chat-will-close" : "Ваш чат будет закрыт через",
  "write-message" : "Введите сообщение...",
  "balance" : "Ваш баланс",
  "name" : "Имя",
  "surname" : "Фамилия",
  "phone" : "Номер телефона",
  "save-data" : "Сохранить данные",
  "add-card" : "Добавить карту",
  "main-card" : "Основная карта",
  "delete" : "Удалить",
  "type-code" : "Введите код для быстрого \nдоступа к приложению",
  "register-code" : "Создайте ваш pin-code для \nприложении",
  "warning" : "Предупреждение",
  "not-working-time" : "Ваша заявка будет обработана в рабочее время. Вы готовы ждать?",
  'profile': 'Профиль',
  'documents-main' : 'документы'

};