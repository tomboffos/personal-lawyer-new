import 'package:personal_lawyer_new/lang/kz_KZ.dart';
import 'package:personal_lawyer_new/lang/ru_RU.dart';

String translate(lang,key){

  if(lang == 'ru')
    return ruRu[key];
  return kzKZ[key];
}