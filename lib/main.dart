import 'dart:convert';
import 'dart:io' show Platform;

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_app_badger/flutter_app_badger.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart';
import 'package:personal_lawyer_new/starts/start.dart';
import 'package:personal_lawyer_new/views/HomePage.dart';
import 'package:personal_lawyer_new/views/authorization/Login.dart';
import 'package:personal_lawyer_new/views/notifications/Firebase.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  Future<void> _firebaseMessagingBackgroundHandler(
      RemoteMessage message) async {
    // If you're going to use other Firebase services in the background, such as Firestore,
    // make sure you call `initializeApp` before using other Firebase services.
    await Firebase.initializeApp();
    FirebaseMessaging messaging = FirebaseMessaging.instance;
    NotificationSettings settings = await messaging.requestPermission(
      alert: true,
      announcement: true,
      badge: true,
      carPlay: true,
      criticalAlert: true,
      provisional: false,
      sound: true,
    );
    String token = await messaging.getToken();
    final prefs = await SharedPreferences.getInstance();
    prefs.setString('device_token', token);
    await FirebaseMessaging.instance
        .setForegroundNotificationPresentationOptions(
      alert: true, // Required to display a heads up notification
      badge: true,
      sound: true,
    );

    if (settings.authorizationStatus == AuthorizationStatus.authorized) {
      print('User granted permission');
    } else if (settings.authorizationStatus ==
        AuthorizationStatus.provisional) {
      print('User granted provisional permission');
    } else {
      print('User declined or has not accepted permission');
    }
    print('User granted permission: ${settings.authorizationStatus}');
    print("Handling a background message: ${message.messageId}");

    FirebaseMessaging.onBackgroundMessage((message) {
      print(message);
      return null;
    });
  }

  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);
  FirebaseMessaging.onMessage.listen((RemoteMessage message) {
    print('Got a message whilst in the foreground!');
    print('Message data: ${message.data}');

    if (message.notification != null) {
      print('Message also contained a notification: ${message.notification}');
    }
  });
  runApp(MyHomePage());
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    FlutterStatusbarcolor.setStatusBarColor(Color(0xff0079C2));

    return MaterialApp(
      home: InitApp(),
      debugShowCheckedModeBanner: false,
      builder: (context, child) {
        return MediaQuery(
            data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
            child: child);
      },
      theme: ThemeData(
          primarySwatch: Colors.blue,
          primaryColor: Color(0xff0079C2),
          backgroundColor: Color(0xffF7F7F7),
          accentColor: Color(0xff003484),
          textTheme: TextTheme(
              headline1: GoogleFonts.openSans(
                  textStyle: TextStyle(
                      fontSize: 20,
                      color: Color(0xff414141),
                      fontWeight: FontWeight.w700)),
              headline2: GoogleFonts.openSans(
                  textStyle: TextStyle(
                      fontSize: 16,
                      color: Color(0xff414141),
                      fontWeight: FontWeight.w700)),
              bodyText1: GoogleFonts.openSans(
                  textStyle: TextStyle(
                      fontSize: 14,
                      color: Color(0xff414141),
                      fontWeight: FontWeight.w400)),
              bodyText2: GoogleFonts.openSans(
                  textStyle: TextStyle(
                      fontSize: 16,
                      color: Color(0xff636363),
                      fontWeight: FontWeight.w600)),
              headline4: GoogleFonts.openSans(
                  textStyle: TextStyle(
                      fontSize: 18,
                      color: Color(0xffffffff),
                      fontWeight: FontWeight.w700)),
              headline5: GoogleFonts.openSans(
                  textStyle: TextStyle(
                      fontSize: 14,
                      color: Color(0xffffffff),
                      fontWeight: FontWeight.w600)),
              headline3: GoogleFonts.openSans(
                  textStyle: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.w600,
                color: Color(0xff414141),
              )))),
    );
  }
}

class InitApp extends StatefulWidget {
  const InitApp({Key key}) : super(key: key);

  @override
  _InitAppState createState() => _InitAppState();
}

class _InitAppState extends State<InitApp> {
  FirebaseService service = new FirebaseService();
  void getLocale() async {
    final prefs = await SharedPreferences.getInstance();
    if (prefs.getString('lang') == null) prefs.setString('lang', 'ru');
  }

  @override
  void initState() {
    // TODO: implement initState
    service.init();

    getLocale();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return StartScreen();
  }
}
