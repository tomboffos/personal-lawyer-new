class User {
  final phone;
  final avatar;
  final name;
  final surname;
  final id;

  User(this.phone, this.avatar, this.name, this.surname, this.id);

  factory User.fromJson(Map<String, dynamic> json){
    return User(json['phone'] , json['avatar'], json['name'], json['surname'], json['id']);
  }
}