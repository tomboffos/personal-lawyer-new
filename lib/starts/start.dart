import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart';
import 'package:personal_lawyer_new/views/HomePage.dart';
import 'package:personal_lawyer_new/views/authorization/Login.dart';
import 'package:personal_lawyer_new/views/authorization/PinCode.dart';
import 'package:shared_preferences/shared_preferences.dart';

class StartScreen extends StatefulWidget {
  const StartScreen({Key key}) : super(key: key);

  @override
  _StartScreenState createState() => _StartScreenState();
}

class _StartScreenState extends State<StartScreen> {
  int index = 0;

  increment() {
    setState(() {
      index += 1;
    });
    if (index == 2)
      Navigator.pushAndRemoveUntil(context,
          MaterialPageRoute(builder: (context) => Login()), (route) => false);
  }

  checkToken() async {
    final prefs = await SharedPreferences.getInstance();
    final token = prefs.getString('token');
    final pinCode = prefs.getString('code');
    final response = await get(Uri.parse('${apiEndpoint}user'),
        headers: {"Authorization": "Bearer $token"});
    print(token != null);
    print(response.statusCode);
    print(pinCode != null);
    if (token != null && pinCode != null && response.statusCode == 200) {
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
              builder: (context) => PinCodeRegisterPage(
                    isLogin: 1,
                  )),
          (route) => false);
    }
  }

  void getLocale() async {
    final prefs = await SharedPreferences.getInstance();
    if (prefs.getString('lang') == null) prefs.setString('lang', 'ru');
  }

  @override
  void initState() {
    // TODO: implement initState
    checkToken();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.only(top: 60),
              child: Center(
                child: Image.asset(
                  'assets/contact-logo.png',
                  height: 119,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 40),
              child: Center(
                child: Image.asset(
                  index == 0 ? 'assets/logo-group.png' : 'assets/Group-2.png',
                  height: MediaQuery.of(context).size.height / 4,
                  width: 289,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 26, left: 30, right: 30),
              child: Center(
                child: Text(
                  index == 0
                      ? 'Для Вашего удобства, шаблоны юридических документов на двух языках!'
                      : 'Дорогой друг! \n Вас приветствует юридическое \n мобильное приложение',
                  textAlign: TextAlign.center,
                  style: GoogleFonts.montserrat(
                      fontSize: 18,
                      fontWeight: FontWeight.w800,
                      color: Color(0xff0079C2)),
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 10),
              child: Text(
                index == 0
                    ? 'Быстро, оперативно, качественно!'
                    : '«Ваш личный адвокат»',
                style: GoogleFonts.montserrat(
                    fontSize: 18,
                    fontWeight: FontWeight.w800,
                    color: Color(0xff37B34A)),
              ),
            ),
            index == 1
                ? Container(
                    margin: EdgeInsets.only(top: 26, left: 30, right: 30),
                    child: Center(
                      child: Text(
                        'Онлайн-консультации с обратной связью!',
                        textAlign: TextAlign.center,
                        style: GoogleFonts.montserrat(
                            fontSize: 18,
                            fontWeight: FontWeight.w800,
                            color: Color(0xff0079C2)),
                      ),
                    ),
                  )
                : SizedBox.shrink(),
          ],
        ),
      ),
      bottomNavigationBar: GestureDetector(
        onTap: () => increment(),
        child: Container(
          height: MediaQuery.of(context).size.height / 7,
          padding: EdgeInsets.only(bottom: 40, top: 10),
          decoration: BoxDecoration(
              color: Theme.of(context).primaryColor,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(75), topRight: Radius.circular(75))),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                  margin: EdgeInsets.only(bottom: 5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 10),
                        child: CircleAvatar(
                          backgroundColor:
                              index == 0 ? Color(0xff37B34A) : Colors.white,
                          radius: 12,
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 10),
                        child: CircleAvatar(
                          backgroundColor:
                              index == 1 ? Color(0xff37B34A) : Colors.white,
                          radius: 12,
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 10),
                        child: CircleAvatar(
                          backgroundColor: Colors.white,
                          radius: 12,
                        ),
                      )
                    ],
                  )),
              Text(
                'Далее',
                style: GoogleFonts.montserrat(
                    color: Colors.white,
                    fontSize: 20,
                    fontWeight: FontWeight.w800),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
