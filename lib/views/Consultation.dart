import 'dart:convert';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:personal_lawyer_new/views/HomePage.dart';
import 'package:personal_lawyer_new/views/payment/PayQuestion.dart';
import 'package:personal_lawyer_new/views/profile/Card.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

import '../language_service.dart';
import 'main_screens/Messages.dart';

class ConsultationQuestion extends StatefulWidget {
  @override
  _ConsultationQuestionState createState() => _ConsultationQuestionState();
}

class _ConsultationQuestionState extends State<ConsultationQuestion> {
  bool lawyer = true;

  bool hideSlide = false;
  bool lawyerChoose = false;
  Map<String, dynamic> choosedLawyer;
  final double _initFabHeight = 120.0;
  double _fabHeight;
  double _panelHeightOpen;
  double _panelHeightClosed = 95.0;
  TextEditingController question_title = new TextEditingController();
  bool buttonLoading = false;
  bool useBonuses = false;
  List<dynamic> lawyers = [];

  Map<String, dynamic> user;
  checkToken() async {
    final prefs = await SharedPreferences.getInstance();
    final token = prefs.getString('token');
    final pinCode = prefs.getString('code');
    final response = await get(Uri.parse('${apiEndpoint}user'),
        headers: {"Authorization": "Bearer $token"});
    print(response.statusCode);
    if (response.statusCode == 200)
      setState(() {
        user = jsonDecode(response.body)['data'];
      });
  }

  fetchCounsels() async {
    final token = (await SharedPreferences.getInstance()).get('token');

    final response = await get(Uri.parse('${apiEndpoint}user/counsels'),
        headers: {
          'Authorization': 'Bearer $token',
          'Accept': 'application/json'
        });
    print(response.body);
    setState(() {
      lawyers = jsonDecode(response.body)['data'];
      hideSlide = true;
      lawyer = false;
    });
  }

  sendQuestion() async {
    setState(() {
      buttonLoading = true;
    });
    final prefs = await SharedPreferences.getInstance();
    final token = prefs.getString('token');

    final response = await post(Uri.parse('${apiEndpoint}questions'), headers: {
      "Authorization": "Bearer $token",
      "Accept": "application/json"
    }, body: {
      "question_title": question_title.text,
      "service_id": "2",
      "use_bonuses": useBonuses ? "1" : "0",
    });

    print(response.body);
    print(response.statusCode);

    if (response.statusCode == 200) {
      setState(() {
        buttonLoading = false;
      });
      final body = jsonDecode(response.body);
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => PayQuestion(
                    data: body['payment'],
                  )));
    } else if (response.statusCode == 201) {
      setState(() {
        buttonLoading = false;
      });
      AwesomeDialog(
          context: context,
          borderSide: BorderSide(color: Colors.green, width: 2),
          width: 500,
          buttonsBorderRadius: BorderRadius.all(Radius.circular(2)),
          headerAnimationLoop: true,
          animType: AnimType.BOTTOMSLIDE,
          title: 'Успешно',
          desc:
              'Спасибо за обращение ответ от юриста будет предоставлен в течении часа',
          showCloseIcon: false,
          useRootNavigator: true,
          dialogType: DialogType.SUCCES,
          btnOkOnPress: () {
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => Messages()));
          })
        ..show();
    } else if (response.statusCode == 404) {
      setState(() {
        buttonLoading = false;
      });
      pushNewScreen(context, screen: CardsPage(), withNavBar: false);
    } else {
      final body = jsonDecode(response.body);
      print(body);
      setState(() {
        buttonLoading = false;
      });
      AwesomeDialog(
        context: context,
        borderSide: BorderSide(color: Colors.green, width: 2),
        width: 500,
        buttonsBorderRadius: BorderRadius.all(Radius.circular(2)),
        headerAnimationLoop: true,
        animType: AnimType.BOTTOMSLIDE,
        title: 'Ошибка',
        desc: '${body['errors']['question_title'][0]}',
        showCloseIcon: false,
        dialogType: DialogType.ERROR,
      )..show();
    }
  }

  sendCounselQuestion() async {
    setState(() {
      buttonLoading = true;
    });
    final prefs = await SharedPreferences.getInstance();
    final token = prefs.getString('token');

    final response =
        await post(Uri.parse('${apiEndpoint}user/counsels'), headers: {
      "Authorization": "Bearer $token",
      "Accept": "application/json"
    }, body: {
      "question_title": question_title.text,
      "service_id": "3",
      "lawyer": choosedLawyer['id'].toString(),
    });

    print(response.body);
    if (response.statusCode == 200) {
      setState(() {
        buttonLoading = false;
      });
      final body = jsonDecode(response.body);
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => PayQuestion(
                    data: body['payment'],
                  )));
    } else if (response.statusCode == 404) {
      setState(() {
        buttonLoading = false;
      });
      pushNewScreen(context, screen: CardsPage(), withNavBar: false);
    } else {
      final body = jsonDecode(response.body);
      print(body);
      setState(() {
        buttonLoading = false;
      });
      AwesomeDialog(
        context: context,
        borderSide: BorderSide(color: Colors.green, width: 2),
        width: 500,
        buttonsBorderRadius: BorderRadius.all(Radius.circular(2)),
        headerAnimationLoop: true,
        animType: AnimType.BOTTOMSLIDE,
        title: 'Ошибка',
        desc: '${body['errors']['question_title'][0]}',
        showCloseIcon: false,
        dialogType: DialogType.ERROR,
      )..show();
    }
  }

  String language;

  void getLocale() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      language = prefs.getString('lang');
    });
    print(language);
  }

  @override
  void initState() {
    // TODO: implement initState
    getLocale();
    checkToken();
    super.initState();
    _fabHeight = _initFabHeight;
  }

  @override
  Widget build(BuildContext context) {
    var w = MediaQuery.of(context).size.width;
    var h = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text(
          translate(language, 'your-question'),
          style: Theme.of(context).textTheme.headline3,
        ),
        centerTitle: false,
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: GestureDetector(
        onTap: () => FocusScope.of(context).requestFocus(new FocusNode()),
        child: SingleChildScrollView(
          child: Stack(
            children: [
              Container(
                padding: EdgeInsets.only(top: 20, left: 16, right: 16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      translate(language, "connect"),
                      style: Theme.of(context).textTheme.headline3,
                    ),
                    Container(
                      width: w - 32,
                      margin: EdgeInsets.only(top: 20),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          border:
                              Border.all(color: Color(0xffDCDCDC), width: 1),
                          borderRadius: BorderRadius.circular(15)),
                      child: Column(
                        children: [
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                lawyer = true;
                                hideSlide = false;
                              });
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                  border: Border(
                                      bottom: BorderSide(
                                          color: Color(0xffDCDCDC)))),
                              padding: EdgeInsets.symmetric(
                                  horizontal: 14, vertical: 14),
                              child: Row(
                                children: [
                                  Container(
                                    width: 20,
                                    height: 20,
                                    padding: EdgeInsets.all(2),
                                    decoration: BoxDecoration(
                                        border: Border.all(
                                            color: lawyer
                                                ? Theme.of(context).primaryColor
                                                : Color(0xff666666),
                                            width: 2),
                                        borderRadius:
                                            BorderRadius.circular(50)),
                                    child: lawyer
                                        ? Container(
                                            decoration: BoxDecoration(
                                                color: Theme.of(context)
                                                    .primaryColor,
                                                borderRadius:
                                                    BorderRadius.circular(50)),
                                          )
                                        : SizedBox.shrink(),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(left: 12),
                                    child: Text(
                                      '${translate(language, "lawyer")} - 3000 тенге',
                                      style: GoogleFonts.openSans(
                                          textStyle: TextStyle(
                                              fontSize: 18,
                                              fontWeight: FontWeight.w400,
                                              color: lawyer
                                                  ? Color(0xff00639E)
                                                  : Color(0xff666666))),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          // GestureDetector(
                          //   onTap: () {
                          //     fetchCounsels();
                          //   },
                          //   child: Container(
                          //     decoration: BoxDecoration(
                          //         border: Border(
                          //             bottom: BorderSide(
                          //                 color: Color(0xffDCDCDC)))),
                          //     padding: EdgeInsets.symmetric(
                          //         horizontal: 14, vertical: 14),
                          //     child: Row(
                          //       children: [
                          //         Container(
                          //           width: 20,
                          //           height: 20,
                          //           padding: EdgeInsets.all(2),
                          //           decoration: BoxDecoration(
                          //               border: Border.all(
                          //                   color: !lawyer
                          //                       ? Theme.of(context).primaryColor
                          //                       : Color(0xff666666),
                          //                   width: 2),
                          //               borderRadius:
                          //                   BorderRadius.circular(50)),
                          //           child: !lawyer
                          //               ? Container(
                          //                   decoration: BoxDecoration(
                          //                       color: Theme.of(context)
                          //                           .primaryColor,
                          //                       borderRadius:
                          //                           BorderRadius.circular(50)),
                          //                 )
                          //               : SizedBox.shrink(),
                          //         ),
                          //         Container(
                          //           margin: EdgeInsets.only(left: 12),
                          //           child: Text(
                          //             'Адвокат - 5000 тенге',
                          //             style: GoogleFonts.openSans(
                          //                 textStyle: TextStyle(
                          //                     fontSize: 18,
                          //                     fontWeight: FontWeight.w400,
                          //                     color: !lawyer
                          //                         ? Color(0xff00639E)
                          //                         : Color(0xff666666))),
                          //           ),
                          //         )
                          //       ],
                          //     ),
                          //   ),
                          // )
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 28, bottom: 20),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(30),
                        border: Border.all(color: Color(0xffDCDCDC), width: 1),
                      ),
                      child: Column(
                        children: [
                          lawyerChoose == true
                              ? Container(
                                  padding: EdgeInsets.symmetric(
                                      vertical: 15, horizontal: 16),
                                  child: Row(
                                    children: [
                                      Text('Ваш адвокат'),
                                      GestureDetector(
                                          onTap: () {
                                            setState(() {
                                              hideSlide = true;
                                            });
                                          },
                                          child: Icon(Icons.edit))
                                    ],
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                  ),
                                )
                              : SizedBox.shrink(),
                          lawyerChoose == true
                              ? Container(
                                  padding: EdgeInsets.symmetric(
                                      vertical: 16, horizontal: 20),
                                  decoration: BoxDecoration(
                                    border: Border(
                                        top: BorderSide(
                                          color: Color(0xffC4C4C4),
                                        ),
                                        bottom: BorderSide(
                                          color: Color(0xffC4C4C4),
                                        )),
                                  ),
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      CircleAvatar(
                                        radius: 22,
                                        foregroundColor: Colors.grey,
                                        foregroundImage: NetworkImage(
                                            choosedLawyer['avatar']),
                                      ),
                                      SizedBox(width: 16),
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Container(
                                            margin: EdgeInsets.only(bottom: 7),
                                            child: Text(
                                              '${choosedLawyer['name']}',
                                              style: GoogleFonts.openSans(
                                                  fontSize: 16,
                                                  fontWeight: FontWeight.w600,
                                                  color: Colors.black),
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(bottom: 7),
                                            child: Text(
                                              'г. Алматы',
                                              style: GoogleFonts.openSans(
                                                  fontSize: 14,
                                                  fontWeight: FontWeight.w400,
                                                  color: Color(0xff505050)),
                                            ),
                                          ),
                                          Container(
                                            child: Text(
                                              'Коллегия: ${choosedLawyer['collegium']}',
                                              style: GoogleFonts.openSans(
                                                  fontSize: 14,
                                                  fontWeight: FontWeight.w400,
                                                  color: Color(0xff505050)),
                                            ),
                                          )
                                        ],
                                      )
                                    ],
                                  ),
                                )
                              : SizedBox.shrink(),
                          Container(
                            padding: EdgeInsets.symmetric(horizontal: 20),
                            child: TextField(
                              maxLines: 8,
                              controller: question_title,
                              decoration: InputDecoration(
                                hintText: translate(language, "interest"),
                                border: InputBorder.none,
                                focusedBorder: InputBorder.none,
                                enabledBorder: InputBorder.none,
                                errorBorder: InputBorder.none,
                                disabledBorder: InputBorder.none,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    user != null
                        ? Container(
                            width: MediaQuery.of(context).size.width - 40,
                            margin: EdgeInsets.symmetric(vertical: 20),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                    'Использовать бонусы (${user['balance']}) '),
                                Checkbox(
                                  onChanged: (value) {
                                    setState(() {
                                      useBonuses = value;
                                    });
                                  },
                                  value: useBonuses,
                                )
                              ],
                            ),
                          )
                        : SizedBox.shrink(),
                    Container(
                      margin: EdgeInsets.only(top: 40),
                      child: Center(
                        child: RaisedButton(
                          padding: EdgeInsets.symmetric(
                              vertical: 20, horizontal: 70),
                          onPressed: () {
                            if (DateTime.now().hour >= 20 &&
                                DateTime.now().minute >= 0) {
                              AwesomeDialog(
                                  context: context,
                                  borderSide:
                                      BorderSide(color: Colors.green, width: 2),
                                  useRootNavigator: true,
                                  width: 500,
                                  buttonsBorderRadius:
                                      BorderRadius.all(Radius.circular(2)),
                                  headerAnimationLoop: true,
                                  animType: AnimType.BOTTOMSLIDE,
                                  title: '${translate(language, 'warning')}',
                                  desc:
                                      '${translate(language, "not-working-time")}',
                                  showCloseIcon: false,
                                  dialogType: DialogType.WARNING,
                                  btnOkOnPress: () => sendQuestion(),
                                  btnCancelOnPress: () => {},
                                  btnCancelText: ' Отменить',
                                  btnOkText: 'Ок')
                                ..show();
                              return false;
                            }

                            if (DateTime.now().hour < 9 &&
                                DateTime.now().minute >= 0) {
                              AwesomeDialog(
                                  context: context,
                                  borderSide:
                                      BorderSide(color: Colors.green, width: 2),
                                  useRootNavigator: true,
                                  width: 500,
                                  buttonsBorderRadius:
                                      BorderRadius.all(Radius.circular(2)),
                                  headerAnimationLoop: true,
                                  animType: AnimType.BOTTOMSLIDE,
                                  title: '${translate(language, 'warning')}',
                                  desc:
                                      '${translate(language, "not-working-time")}',
                                  showCloseIcon: false,
                                  dialogType: DialogType.WARNING,
                                  btnCancelText: ' Отменить',
                                  btnOkOnPress: () => sendQuestion(),
                                  btnCancelOnPress: () => {})
                                ..show();
                              return false;
                            }

                            if (!buttonLoading && choosedLawyer == null)
                              sendQuestion();
                            if (!buttonLoading && choosedLawyer != null)
                              sendCounselQuestion();
                          },
                          color: Theme.of(context).primaryColor,
                          child: buttonLoading
                              ? CircularProgressIndicator()
                              : Text(
                                  translate(language, "pay"),
                                  style: GoogleFonts.openSans(
                                      fontSize: 16,
                                      fontWeight: FontWeight.w400,
                                      color: Colors.white),
                                ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              hideSlide == true
                  ? Positioned(
                      child: SlidingUpPanel(
                        renderPanelSheet: false,
                        panel: Container(
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.only(
                                  topRight: Radius.circular(30),
                                  topLeft: Radius.circular(30)),
                              boxShadow: [
                                BoxShadow(
                                  blurRadius: 20.0,
                                  color: Colors.grey,
                                ),
                              ]),
                          child: Center(
                            child: Column(
                              children: [
                                Container(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 20, vertical: 16),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        'Выберите адвоката',
                                        style: GoogleFonts.openSans(
                                            fontSize: 16,
                                            fontWeight: FontWeight.w600,
                                            color: Colors.black),
                                      ),
                                      GestureDetector(
                                        onTap: () {
                                          setState(() {
                                            hideSlide = false;
                                            lawyer = true;
                                          });
                                        },
                                        child: Text(
                                          'Отмена',
                                          style: GoogleFonts.openSans(
                                              fontWeight: FontWeight.w400,
                                              fontSize: 14,
                                              color: Color(0xff414141)),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                Container(
                                  height:
                                      MediaQuery.of(context).size.height / 3,
                                  child: ListView.builder(
                                      itemBuilder: (context, index) {
                                        return GestureDetector(
                                          onTap: () {
                                            setState(() {
                                              hideSlide = false;
                                              lawyerChoose = true;
                                              choosedLawyer = lawyers[index];
                                            });
                                          },
                                          child: Container(
                                            padding: EdgeInsets.symmetric(
                                                vertical: 16, horizontal: 20),
                                            decoration: BoxDecoration(
                                              border: Border(
                                                  top: BorderSide(
                                                color: Color(0xffC4C4C4),
                                              )),
                                            ),
                                            child: Row(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                CircleAvatar(
                                                  radius: 30,
                                                  foregroundColor: Colors.grey,
                                                  foregroundImage: NetworkImage(
                                                      lawyers[index]['avatar']),
                                                ),
                                                SizedBox(
                                                  width: 20,
                                                ),
                                                Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Container(
                                                      margin: EdgeInsets.only(
                                                          bottom: 7),
                                                      child: Text(
                                                        lawyers[index]['name'],
                                                        style: GoogleFonts
                                                            .openSans(
                                                                fontSize: 16,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w600,
                                                                color: Colors
                                                                    .black),
                                                      ),
                                                    ),
                                                    Container(
                                                      margin: EdgeInsets.only(
                                                          bottom: 7),
                                                      child: Text(
                                                        'г. Алматы',
                                                        style: GoogleFonts
                                                            .openSans(
                                                                fontSize: 14,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w400,
                                                                color: Color(
                                                                    0xff505050)),
                                                      ),
                                                    ),
                                                    Container(
                                                      child: Text(
                                                        'Коллегия: ${lawyers[index]['collegium']}',
                                                        style: GoogleFonts
                                                            .openSans(
                                                                fontSize: 14,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w400,
                                                                color: Color(
                                                                    0xff505050)),
                                                      ),
                                                    )
                                                  ],
                                                )
                                              ],
                                            ),
                                          ),
                                        );
                                      },
                                      itemCount: lawyers.length),
                                )
                              ],
                            ),
                          ),
                        ),
                        collapsed: _floatingCollapsed(),
                        backdropEnabled: true,
                        defaultPanelState: PanelState.OPEN,
                        onPanelClosed: () {
                          setState(() {
                            hideSlide = false;
                            lawyer = true;
                          });
                        },
                      ),
                    )
                  : SizedBox.shrink()
            ],
          ),
        ),
      ),
    );
  }
}

Widget _floatingCollapsed() {
  return Container(
    decoration: BoxDecoration(
      color: Colors.blueGrey,
      borderRadius: BorderRadius.only(
          topLeft: Radius.circular(24.0), topRight: Radius.circular(24.0)),
    ),
    margin: const EdgeInsets.fromLTRB(24.0, 24.0, 24.0, 0.0),
    child: Center(
      child: Text(
        "This is the collapsed Widget",
        style: TextStyle(color: Colors.white),
      ),
    ),
  );
}

Widget _floatingPanel() {
  return Container();
}
