import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:personal_lawyer_new/views/Consultation.dart';
import 'package:personal_lawyer_new/views/documents/DocumentPurchase.dart';
import 'package:personal_lawyer_new/views/documents/DocumentsPage.dart';
import 'package:personal_lawyer_new/views/main_screens/HomeScreen.dart';
import 'package:personal_lawyer_new/views/main_screens/Messages.dart';
import 'package:personal_lawyer_new/views/profile/Documents.dart';
import 'package:personal_lawyer_new/views/profile/Profile.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}
final apiEndpoint = 'https://personal-lawyer.it-lead.net/api/';
class _HomePageState extends State<HomePage> {
  List<PersistentBottomNavBarItem> _navBarsItem(){
    return [
      PersistentBottomNavBarItem(
        icon: Icon(Icons.dashboard),
        title: ('Главная'),
        activeColorPrimary: Colors.white,
        inactiveColorPrimary: Theme.of(context).accentColor,




      ),
      PersistentBottomNavBarItem(
        icon: Icon(Icons.mail),
        title: ('Сообщение'),
        activeColorPrimary: Colors.white,

        inactiveColorPrimary: Theme.of(context).accentColor,



      ),
      PersistentBottomNavBarItem(
        icon: Icon(Icons.contact_page_sharp),
        title: ('Мои документы'),
        activeColorPrimary: Colors.white,
        inactiveColorPrimary: Theme.of(context).accentColor,



      ),
      PersistentBottomNavBarItem(
        icon: Icon(Icons.person),
        title: ('Мой профиль'),
        activeColorPrimary: Colors.white,
        inactiveColorPrimary: Theme.of(context).accentColor,



      ),

    ];
  }
  PersistentTabController _controller;


  int _viewIndex;
  List<Widget> _buildScreens(){
    return [
      HomeScreen(),
      Messages(),
      UserDocuments(),
      Profile(),


    ];
  }

  @override
  void initState() {
    // TODO: implement initState
    _viewIndex = 0;
    _controller = PersistentTabController(initialIndex: 0);

    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    var w = MediaQuery.of(context).size.width;
    var h = MediaQuery.of(context).size.height;




    return Scaffold(
      body: PersistentTabView(
        context,
        controller: _controller,
        screens: _buildScreens(),
        items: _navBarsItem(),
        backgroundColor: Color(0xff0079C2),
        handleAndroidBackButtonPress: true,
        confineInSafeArea: true,
        resizeToAvoidBottomInset: true,
        onItemSelected: (int) {

          setState(() {}); // This is required to update the nav bar if Android back button is pressed
        },
        stateManagement: true,
        hideNavigationBarWhenKeyboardShows: true,
        decoration: NavBarDecoration(
            borderRadius: BorderRadius.circular(0)
        ),
        popAllScreensOnTapOfSelectedTab: true,
        popActionScreens: PopActionScreensType.all,
        navBarStyle: NavBarStyle.style6,




      ),
    );
  }
}
