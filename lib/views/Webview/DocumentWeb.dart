import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
class DocumentWeb extends StatefulWidget {
  final data;
  const DocumentWeb({Key key, this.data}) : super(key: key);

  @override
  _DocumentWebState createState() => _DocumentWebState();
}

class _DocumentWebState extends State<DocumentWeb> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: AppBar(
        backgroundColor: Colors.white,
        centerTitle: true,
        elevation: 1,
        title: Text(
          'Мои документ',
          style: Theme.of(context).textTheme.headline3,
        ),
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: WebviewScaffold(
          url: this.widget.data
      ),
    );
  }
}
