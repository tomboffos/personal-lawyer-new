import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart';
import 'package:personal_lawyer_new/views/authorization/PhoneConfirmation.dart';

import '../HomePage.dart';

class EnterNumber extends StatefulWidget {
  final isForgot;

  const EnterNumber({Key key, this.isForgot}) : super(key: key);
  @override
  _EnterNumberState createState() => _EnterNumberState();
}

class _EnterNumberState extends State<EnterNumber> {
  TextEditingController phoneController = new MaskedTextController(mask: '+0 (000) 000-00-00', text: '+7');
  bool loading = false;
  login(Map<String, dynamic> form) async {
    setState(() {
      loading = true;
    });
    final response = await post(
        Uri.parse('${apiEndpoint}auth/login'), headers: {
      "Accept": "application/json"
    }, body: form);

    final body = jsonDecode(response.body);

    print(body);
    if (body['data'] != null){
      setState(() {
        loading = false;
      });
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) =>
                  PhoneConfirmation(
                    user: body['data'],
                    isLogin: 1,
                  )));
    }
  }

  @override
  Widget build(BuildContext context) {
    var w = MediaQuery.of(context).size.width;

    return Scaffold(
      backgroundColor: Color(0xFFF7F7F7),
      appBar: AppBar(

        backgroundColor: Color(0xFFF7F7F7),
        elevation: 0,
        title: Text('${this.widget.isForgot == 0 ? 'Вход' :'Укажите номер телефона'}',style: Theme.of(context).textTheme.headline2,),
        centerTitle: false,
        iconTheme: IconThemeData(

          color: this.widget.isForgot == 0 ? Colors.white : Colors.black
        ),
        automaticallyImplyLeading: false,


      ),
      body: Container(
        padding: EdgeInsets.symmetric(vertical: 24,horizontal: 16),

        child: Column(
          children: [
            TextField(
              controller: phoneController,
              textInputAction: TextInputAction.send,
              keyboardType: TextInputType.phone,
              decoration: InputDecoration(
                  labelText: 'Телефон',
                  labelStyle: TextStyle(fontSize: 20),
                  border: new OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(100),
                    borderSide:
                    new BorderSide(color: Theme.of(context).primaryColor),
                  )),
            ),
            Container(
              margin: EdgeInsets.only(top: 40),
              child: GestureDetector(
                onTap: () {
                  if(!loading)
                    login({"phone" : phoneController.text});
                },
                child: Container(
                  width: w - 16,
                  alignment: Alignment.center,
                  padding: EdgeInsets.symmetric(vertical: 15),
                  decoration: BoxDecoration(
                    color: Theme.of(context).primaryColor,
                    borderRadius: BorderRadius.circular(25),
                  ),
                  child:loading ? Center(
                    child: CircularProgressIndicator(),
                  ) : Text(
                      'Продолжить',
                      style: GoogleFonts.openSans(
                        textStyle: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                            fontWeight: FontWeight.w600),
                      )),
                ),
              ),
            )


          ],
        ),
      )
    );
  }
}
