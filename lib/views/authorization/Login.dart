import 'dart:convert';
import 'dart:io';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart';
import 'package:personal_lawyer_new/views/HomePage.dart';
import 'package:personal_lawyer_new/views/authorization/PinCode.dart';
import 'package:personal_lawyer_new/views/components/AuthorizationComponent.dart';
import 'package:personal_lawyer_new/views/main_screens/HomeScreen.dart';
import 'package:personal_lawyer_new/views/notifications/Firebase.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  bool login = true;
  final firebase = FirebaseService();

  checkToken() async {
    final prefs = await SharedPreferences.getInstance();
    final token = prefs.getString('token');
    final pinCode = prefs.getString('code');
    final response = await get(Uri.parse('${apiEndpoint}user'),
        headers: {"Authorization": "Bearer $token"});
    print(response.body);

    if (token != null && pinCode != null && response.statusCode == 200) {
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
              builder: (context) => PinCodeRegisterPage(
                    isLogin: 1,
                  )),
          (route) => false);
    }
  }
  void checkVersions() async{
    final response = await get(Uri.parse('${apiEndpoint}versions'));

    final body = jsonDecode(response.body);
    if(iosVersion != body['ios']){
      AwesomeDialog(
          context: context,
          borderSide: BorderSide(color: Colors.green, width: 2),
          width: 500,
          buttonsBorderRadius: BorderRadius.all(Radius.circular(2)),
          headerAnimationLoop: true,
          animType: AnimType.BOTTOMSLIDE,
          title: 'Ошибка',
          desc: 'Обновите приложение',
          showCloseIcon: false,
          dialogType: DialogType.ERROR,
          dismissOnTouchOutside: false,
          btnOkOnPress: ()=>
              launch(Platform.isIOS ?
              'https://apps.apple.com/ru/app/%D0%B2%D0%B0%D1%88-%D0%BB%D0%B8%D1%87%D0%BD%D1%8B%D0%B9-%D0%B0%D0%B4%D0%B2%D0%BE%D0%BA%D0%B0%D1%82/id1499337941' :
              'https://play.google.com/store/apps/details?id=kz.itgroup.personallawyer&hl=ru&gl=US')

      )..show();

    }
  }

  @override
  void initState() {
    // TODO: implement initState
    // firebase.init();

    checkVersions();
    checkToken();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var w = MediaQuery.of(context).size.width;
    var h = MediaQuery.of(context).size.height;

    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: GestureDetector(
        onTap: () => FocusScope.of(context).requestFocus(new FocusNode()),
        child: DefaultTabController(
          length: (2),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  decoration: BoxDecoration(
                    color: Theme.of(context).backgroundColor,
                  ),
                  padding: EdgeInsets.only(top: 150),
                  alignment: Alignment.center,
                  child: Image.asset(
                    'assets/logo.png',
                    height: 100,
                  ),
                ),
                Container(
                    padding: EdgeInsets.only(top: 20),
                    alignment: Alignment.center,
                    child: Text('Ваш личный адвокат',
                        style: GoogleFonts.openSans(
                            textStyle: TextStyle(
                                fontSize: 18,
                                color: Theme.of(context).primaryColor,
                                fontWeight: FontWeight.bold)))),
                Container(
                  height: h - 300,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(65),
                        topRight: Radius.circular(65),
                      )),
                  margin: EdgeInsets.only(top: 30),
                  child: Container(
                      padding: const EdgeInsets.only(top: 10),
                      child: Column(
                        children: [
                          TabBar(
                            labelColor: Color(0xff545454),
                            labelStyle: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.w600),
                            indicatorColor: Color(0xff545454),
                            tabs: [
                              Tab(
                                text: 'Регистрация',
                              ),
                              Tab(
                                text: 'Войти',
                              ),

                            ],
                          ),
                          Container(
                            height: h - h * 0.55,
                            child: TabBarView(children: [

                              AuthorizationComponent(
                                isLogin: 0,
                              ),
                              AuthorizationComponent(
                                isLogin: 1,
                              ),

                            ]),
                          )
                        ],
                      )),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
