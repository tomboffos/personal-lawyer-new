import 'dart:convert';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart';
import 'package:personal_lawyer_new/views/HomePage.dart';
import 'package:personal_lawyer_new/views/authorization/PinCode.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PhoneConfirmation extends StatefulWidget {
  final Map<String, dynamic> user;
  final int isLogin;

  const PhoneConfirmation({Key key, this.user, this.isLogin}) : super(key: key);

  @override
  _PhoneConfirmationState createState() => _PhoneConfirmationState();
}

class _PhoneConfirmationState extends State<PhoneConfirmation> {
  TextEditingController pinController = new TextEditingController();
  bool loading = false;

  void registerCheck(Map<String, dynamic> form) async {
    setState(() {
      loading = true;
    });
    final prefs = await SharedPreferences.getInstance();
    final deviceToken = prefs.getString('device_token');
    form['device_token'] = deviceToken;
    final response = await post(
        Uri.parse(
            '${apiEndpoint}auth/register/check/${this.widget.user['id']}'),
        headers: {
          "Accept": "application/json",
        },
        body: form);
    print(response.body);
    final body = jsonDecode(response.body);
    if (response.statusCode == 200) {
      setState(() {
        loading = false;
      });
      print('TOKEN' + body['token']);
      final prefs = await SharedPreferences.getInstance();
      prefs.setString('token', body['token']);
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => PinCodeRegisterPage(
                    isLogin: 0,
                    isEdit: 0,
                  )));
    } else {
      setState(() {
        loading = false;
      });
      AwesomeDialog(
        context: context,
        borderSide: BorderSide(color: Colors.green, width: 2),
        width: 500,
        buttonsBorderRadius: BorderRadius.all(Radius.circular(2)),
        headerAnimationLoop: true,
        animType: AnimType.BOTTOMSLIDE,
        title: 'Просим прощения',
        desc: 'Сообщение не верно',
        useRootNavigator: true,
        btnOkOnPress: () => {},
        showCloseIcon: false,
        dialogType: DialogType.ERROR,
      )..show();
    }
  }

  loginCheck(code) async {
    setState(() {
      loading = true;
    });
    final prefs = await SharedPreferences.getInstance();
    final deviceToken = prefs.getString('device_token');
    final response = await post(
        Uri.parse('${apiEndpoint}auth/login/check/${this.widget.user['id']}'),
        headers: {"Accept": "application/json"},
        body: {"code": code, "device_token": deviceToken});

    final body = jsonDecode(response.body);
    print(body);
    print(body['token']);

    if (response.statusCode == 200) {
      setState(() {
        loading = false;
      });

      final prefs = await SharedPreferences.getInstance();
      prefs.setString('token', body['token']);
      print(prefs.getString('token'));
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => PinCodeRegisterPage(isLogin: 0)));
    } else {
      setState(() {
        loading = false;
      });
      AwesomeDialog(
        context: context,
        borderSide: BorderSide(color: Colors.green, width: 2),
        width: 500,
        buttonsBorderRadius: BorderRadius.all(Radius.circular(2)),
        headerAnimationLoop: true,
        animType: AnimType.BOTTOMSLIDE,
        title: 'Просим прощения',
        desc: 'Сообщение не верно',
        useRootNavigator: true,
        btnOkOnPress: () => {},
        showCloseIcon: false,
        dialogType: DialogType.ERROR,
      )..show();
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var w = MediaQuery.of(context).size.width;

    return Scaffold(
      backgroundColor: Color(0xFFF7F7F7),
      appBar: AppBar(
        backgroundColor: Theme.of(context).backgroundColor,
        elevation: 0,
        title: Text(
          'Подтверждение номера',
          style: Theme.of(context).textTheme.headline2,
        ),
        centerTitle: false,
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: Container(
        padding: EdgeInsets.only(top: 40, left: 16, right: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Телефон',
              style: Theme.of(context).textTheme.bodyText1,
              textAlign: TextAlign.left,
            ),
            Container(
              padding: const EdgeInsets.all(25.0),
              child: Form(
                  child: PinCodeTextField(
                controller: pinController,
                backgroundColor: Theme.of(context).backgroundColor,
                length: 4,
                appContext: context,
                onChanged: (value) {},
                pinTheme: PinTheme(
                    fieldHeight: 50,
                    fieldWidth: 40,
                    activeColor: Theme.of(context).primaryColor,
                    inactiveColor: Color(0xffC0C0C0)),
                pastedTextStyle: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                ),
              )),
            ),
            Container(
              margin: EdgeInsets.only(top: 36),
              child: Text(
                'Вам отправлено сообщение с 4-х значным кодом, для подтверждения вашего номера телефона',
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.bodyText2,
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 40),
              child: GestureDetector(
                onTap: () {
                  if (!loading) {
                    if (this.widget.isLogin == 1) {
                      loginCheck(pinController.text);
                    } else {
                      registerCheck({"code": pinController.text});
                    }
                  }
                },
                child: Container(
                  width: w - 16,
                  alignment: Alignment.center,
                  padding: EdgeInsets.symmetric(vertical: 15),
                  decoration: BoxDecoration(
                    color: Theme.of(context).primaryColor,
                    borderRadius: BorderRadius.circular(25),
                  ),
                  child: loading
                      ? Center(
                          child: CircularProgressIndicator(),
                        )
                      : Text('Продолжить',
                          style: GoogleFonts.openSans(
                            textStyle: TextStyle(
                                color: Colors.white,
                                fontSize: 20,
                                fontWeight: FontWeight.w600),
                          )),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
