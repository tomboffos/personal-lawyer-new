import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:personal_lawyer_new/views/HomePage.dart';
import 'package:personal_lawyer_new/views/authorization/EnterNumber.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../language_service.dart';

class PinCodeRegisterPage extends StatefulWidget {
  final isLogin;

  final isEdit;
  const PinCodeRegisterPage({Key key, this.isLogin, this.isEdit})
      : super(key: key);
  @override
  _PinCodeRegisterPageState createState() => _PinCodeRegisterPageState();
}

class _PinCodeRegisterPageState extends State<PinCodeRegisterPage> {
  List<Widget> numbers = [];

  String numberCode = '';

  void addNumbers(String number) async {
    if (numberCode.length <= 4) {
      setState(() {
        numberCode = numberCode + '$number';
      });
      if (numberCode.length == 4) {
        final prefs = await SharedPreferences.getInstance();
        if (widget.isLogin != 1) {
          prefs.setString('code', numberCode);
          print(numberCode);

          AwesomeDialog(
              context: context,
              borderSide: BorderSide(color: Colors.green, width: 2),
              width: 500,
              buttonsBorderRadius: BorderRadius.all(Radius.circular(2)),
              headerAnimationLoop: true,
              animType: AnimType.BOTTOMSLIDE,
              title: 'Успешно',
              desc: 'Ваш код успешно сохранен',
              showCloseIcon: true,
              dialogType: DialogType.SUCCES,
              btnOkOnPress: () => Navigator.pop(context))
            ..show();
          if (widget.isLogin == 0) {
            Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(builder: (context) => HomePage()),
                (route) => false);
          }
        } else {
          final pinCode = prefs.getString('code');
          print(pinCode);
          if (pinCode == numberCode)
            Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(builder: (context) => HomePage()),
                (route) => false);
          else
            AwesomeDialog(
                context: context,
                borderSide: BorderSide(color: Colors.green, width: 2),
                width: 500,
                buttonsBorderRadius: BorderRadius.all(Radius.circular(2)),
                headerAnimationLoop: true,
                animType: AnimType.BOTTOMSLIDE,
                title: 'Ошибка',
                desc: 'Не правильный код',
                showCloseIcon: true,
                dialogType: DialogType.ERROR,
                btnOkOnPress: () => {})
              ..show();
        }
        numberCode = '';
      }
    } else {
      setState(() {
        numberCode = '';
      });
    }
  }

  void getNumbers() {
    numbers = [];

    for (var i = 1; i <= 9; i++) {
      numbers.add(
        Container(
          padding: EdgeInsets.only(
              left: MediaQuery.of(context).size.width / 25,
              right: MediaQuery.of(context).size.width / 25,
              bottom: 25),
          child: SizedBox(
            width: 70,
            height: 70,
            child: FloatingActionButton(
              heroTag: 'number-$i',
              onPressed: () => addNumbers(i.toString()),
              shape: CircleBorder(
                  side: BorderSide(color: Theme.of(context).primaryColor)),
              backgroundColor: Colors.white,
              child: Text(
                '$i',
                style: TextStyle(
                  color: Theme.of(context).primaryColor,
                  fontSize: 36,
                ),
              ),
            ),
          ),
        ),
      );
    }

    numbers.add(Container(
      padding: EdgeInsets.only(
          left: MediaQuery.of(context).size.width / 25,
          right: MediaQuery.of(context).size.width / 25,
          bottom: 25),
      child: SizedBox(
        height: 70,
        width: 70,
      ),
    ));
    numbers.add(
      Container(
        padding: EdgeInsets.only(
            left: MediaQuery.of(context).size.width / 25,
            right: MediaQuery.of(context).size.width / 25,
            bottom: 25),
        child: SizedBox(
          width: 70,
          height: 70,
          child: FloatingActionButton(
            heroTag: 'number-0',
            onPressed: () => addNumbers('0'),
            shape: CircleBorder(
                side: BorderSide(color: Theme.of(context).primaryColor)),
            backgroundColor: Colors.white,
            child: Text(
              '0',
              style: TextStyle(
                color: Theme.of(context).primaryColor,
                fontSize: 36,
              ),
            ),
          ),
        ),
      ),
    );

    numbers.add(Container(
      padding: EdgeInsets.only(
          left: MediaQuery.of(context).size.width / 25,
          right: MediaQuery.of(context).size.width / 25,
          bottom: 24),
      child: SizedBox(
        width: 70,
        height: 70,
        child: FloatingActionButton(
            heroTag: 'backspace',
            onPressed: () => deleteNumber(),
            shape: CircleBorder(
                side: BorderSide(color: Theme.of(context).primaryColor)),
            backgroundColor: Colors.white,
            child: Icon(
              Icons.backspace,
              color: Theme.of(context).primaryColor,
            )),
      ),
    ));

    print(numbers);
  }

  String language;
  void getLocale() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      language = prefs.getString('lang');
    });
    print(language);
  }

  @override
  void initState() {
    getLocale();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var w = MediaQuery.of(context).size.width;
    this.getNumbers();

    return Scaffold(
      body: Container(
        padding: EdgeInsets.only(top: 60, left: 16, right: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            this.widget.isLogin == 0
                ? Container(
                    alignment: Alignment.topLeft,
                    child: Text('Pin code',
                        style: Theme.of(context).textTheme.headline1),
                  )
                : SizedBox.shrink(),
            Container(
              alignment: this.widget.isLogin == 0
                  ? Alignment.topLeft
                  : Alignment.center,
              margin: EdgeInsets.only(top: 15),
              child: Text(
                '${this.widget.isLogin == 0 ? '${translate(language, "register-code")}' : '${translate(language, "type-code")}'}',
                style: Theme.of(context).textTheme.bodyText1,
                textAlign: this.widget.isLogin == 1
                    ? TextAlign.center
                    : TextAlign.left,
              ),
            ),
            this.widget.isLogin == 1
                ? GestureDetector(
                    onTap: () => Navigator.push(context,
                        MaterialPageRoute(builder: (context) => EnterNumber())),
                    child: Container(
                      margin: EdgeInsets.only(top: 20),
                      child: Text(
                        'Забыли пароль',
                        style: GoogleFonts.openSans(
                            fontSize: 14,
                            fontWeight: FontWeight.w400,
                            decoration: TextDecoration.underline),
                      ),
                    ),
                  )
                : SizedBox.shrink(),
            Container(
              alignment: Alignment.center,
              margin: EdgeInsets.only(top: 60),
              width: w / 2,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  numberCode.length == 0
                      ? Container(
                          width: 12,
                          height: 12,
                          decoration: BoxDecoration(
                              color: Theme.of(context).primaryColor,
                              borderRadius: BorderRadius.circular(100)),
                        )
                      : Text('${numberCode[0]}'),
                  numberCode.length <= 1
                      ? Container(
                          width: 12,
                          height: 12,
                          decoration: BoxDecoration(
                              color: Theme.of(context).primaryColor,
                              borderRadius: BorderRadius.circular(100)),
                        )
                      : Text('${numberCode[1]}'),
                  numberCode.length <= 2
                      ? Container(
                          width: 12,
                          height: 12,
                          decoration: BoxDecoration(
                              color: Theme.of(context).primaryColor,
                              borderRadius: BorderRadius.circular(100)),
                        )
                      : Text('${numberCode[2]}'),
                  numberCode.length <= 3
                      ? Container(
                          width: 12,
                          height: 12,
                          decoration: BoxDecoration(
                              color: Theme.of(context).primaryColor,
                              borderRadius: BorderRadius.circular(100)),
                        )
                      : Text('${numberCode[3]}')
                ],
              ),
            ),
            Container(
              width: w - 20,
              margin: EdgeInsets.only(top: 60),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Wrap(
                    alignment: WrapAlignment.spaceEvenly,
                    children: numbers,
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  deleteNumber() {
    setState(() {
      numberCode = numberCode.substring(0, numberCode.length - 1);
    });
  }
}
