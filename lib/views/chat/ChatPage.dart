import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_countdown_timer/current_remaining_time.dart';
import 'package:flutter_countdown_timer/flutter_countdown_timer.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart';
import 'package:personal_lawyer_new/views/HomePage.dart';
import 'package:personal_lawyer_new/views/chat/Lawyer.dart';
import 'package:pusher_client/pusher_client.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../language_service.dart';

class ChatPage extends StatefulWidget {
  final role;
  final Map<String, dynamic> question;

  const ChatPage({Key key, this.question, this.role}) : super(key: key);

  @override
  _ChatPageState createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> {
  List<dynamic> messages = [];
  bool loading = false;

  initListenerPusher() {
    PusherClient pusher =
        PusherClient('71b7e222b90fdffa56e6', PusherOptions(cluster: 'ap2'));

    pusher.connect();
    Channel channel = pusher.subscribe("App.Chat.${widget.question['id']}");

    channel.bind('message-created', (PusherEvent event) {
      print(event.toString() + 'hi');
      loadMessages(1);
    });
    pusher.onConnectionStateChange((state) {
      print(
          "previousState: ${state.previousState},\n currentState: ${state.currentState}");
    });
  }

  loadMessages(withoutLoading) async {
    final prefs = await SharedPreferences.getInstance();
    final token = prefs.getString('token');
    final deviceToken = prefs.getString('device_token');
    print(deviceToken);
    if (withoutLoading == null) {
      setState(() {
        loading = true;
      });
    }
    final response = await get(
        Uri.parse('${apiEndpoint}user/questions/${widget.question['id']}'),
        headers: {"Authorization": "Bearer $token"});
    if (response.statusCode == 200) {
      final body = jsonDecode(response.body);

      setState(() {
        if (withoutLoading == null) {
          loading = false;
        }
        messages = body['data'];
      });
    }
  }

  TextEditingController message = new TextEditingController();

  sendMessage() async {
    if (message.text.length < 1) return false;

    final prefs = await SharedPreferences.getInstance();
    final token = prefs.getString('token');

    final response = await post(Uri.parse('${apiEndpoint}user/questions'),
        headers: {
          "Authorization": "Bearer $token"
        },
        body: {
          "message": message.text,
          "order_id": widget.question['id'].toString()
        });
    print(response.statusCode);

    if (response.statusCode == 201) {
      message.text = '';
      await loadMessages(1);
    }
  }

  Map<String, dynamic> user;

  getUserData() async {
    final prefs = await SharedPreferences.getInstance();
    final token = prefs.getString('token');
    final response = await get(Uri.parse('${apiEndpoint}user'),
        headers: {"Authorization": "Bearer $token"});
    if (response.statusCode == 200) {
      final body = jsonDecode(response.body);
      setState(() {
        user = body['data'];
      });
    }
  }

  String language;
  void getLocale() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      language = prefs.getString('lang');
    });
    print(language);
  }

  @override
  void initState() {
    // TODO: implement initState
    loadMessages(null);
    getUserData();
    initListenerPusher();
    getLocale();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var w = MediaQuery.of(context).size.width;
    var h = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        elevation: 1,
        centerTitle: false,
        title: GestureDetector(
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => Lawyer(
                        user: widget.question['lawyer'],
                        order: widget.question)));
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                width: 32,
                height: 32,
                margin: EdgeInsets.only(right: 12),
                decoration:
                    BoxDecoration(borderRadius: BorderRadius.circular(100)),
                child: Image(
                  image: widget.role != 2
                      ? NetworkImage('${widget.question['user']['avatar']}')
                      : NetworkImage('${widget.question['lawyer']['avatar']}'),
                  fit: BoxFit.cover,
                ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    constraints: BoxConstraints(
                        maxWidth: MediaQuery.of(context).size.width),
                    child: Text(
                      widget.role == 2
                          ? '${widget.question['lawyer']['name'].length > 30 ? widget.question['lawyer']['name'].substring(0, 30) + '...' : widget.question['lawyer']['name']}'
                          : '${widget.question['user']['name'].length > 30 ? widget.question['user']['name'].substring(0, 30) + '...' : widget.question['user']['name']}',
                      style: Theme.of(context).textTheme.headline3,
                    ),
                  ),
                  Text(
                    widget.role != 2
                        ? 'Пользователь'
                        : translate(language, 'lawyer'),
                    style: GoogleFonts.openSans(
                        textStyle: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.w400,
                            color: Colors.black)),
                  )
                ],
              ),
            ],
          ),
        ),
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              height: h - h * 0.85,
              child: Stack(
                children: [
                  Container(
                    padding: EdgeInsets.only(
                      left: 47,
                      right: 47,
                      top: 15,
                    ),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                            bottomRight: Radius.circular(30),
                            bottomLeft: Radius.circular(30))),
                    child: Container(
                      margin: EdgeInsets.only(bottom: 50),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          widget.question['order_status']['id'] != 4
                              ? Icon(Icons.watch_later_outlined)
                              : SizedBox.shrink(),
                          Text(
                            "${widget.question['order_status']['id'] != 4 ? translate(language, "chat-will-close") : translate(language, "chat-closed")}",
                            style: GoogleFonts.openSans(
                                fontSize: 14,
                                fontWeight: FontWeight.w400,
                                color: Color(0xff505050)),
                          ),
                          widget.question['order_status']['id'] != 4
                              ? Container(
                                  padding: EdgeInsets.symmetric(
                                      vertical: 5, horizontal: 10),
                                  decoration: BoxDecoration(
                                      color: Color(0xff1F7E8B),
                                      borderRadius: BorderRadius.circular(50)),
                                  child: CountdownTimer(
                                    endTime: DateTime.parse(
                                                widget.question['end_time'])
                                            .millisecondsSinceEpoch +
                                        1000 * 30,
                                    widgetBuilder:
                                        (_, CurrentRemainingTime time) {
                                      return Text(
                                        '${time.hours != null ? '${time.hours}ч:' : ''}${time.min != null ? '${time.min}м:' : ''}${time.sec}c',
                                        style: GoogleFonts.openSans(
                                            fontSize: 12,
                                            fontWeight: FontWeight.w400,
                                            color: Colors.white),
                                      );
                                    },
                                  ),
                                )
                              : SizedBox.shrink()
                        ],
                      ),
                    ),
                  ),
                  widget.role != 2
                      ? SizedBox.shrink()
                      : Positioned(
                          bottom: 15,
                          left: w - w * 0.75,
                          child: GestureDetector(
                            onTap: () {
                              print(widget.question['lawyer']['phone']
                                  .replaceAll(' ', ''));
                              launch(
                                  'tel://${widget.question['lawyer']['phone'].replaceAll(' ', '')}');
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Container(
                                  child: Row(
                                    children: [
                                      Container(
                                        child: Icon(
                                          Icons.phone,
                                          color: Colors.white,
                                        ),
                                        padding: EdgeInsets.only(
                                            bottom: 12,
                                            top: 12,
                                            right: 8,
                                            left: 10),
                                        decoration: BoxDecoration(
                                            color: Color(0xff18BA45),
                                            borderRadius: BorderRadius.only(
                                                bottomLeft:
                                                    Radius.circular(100),
                                                topLeft: Radius.circular(100))),
                                      ),
                                      Container(
                                        padding: EdgeInsets.only(
                                            bottom: 13,
                                            top: 13,
                                            right: 12,
                                            left: 12),
                                        margin: EdgeInsets.only(left: 2),
                                        child: Text(
                                          '${widget.question['lawyer']['phone']}',
                                          style: GoogleFonts.openSans(
                                              fontSize: 16,
                                              fontWeight: FontWeight.w400,
                                              color: Colors.white),
                                        ),
                                        decoration: BoxDecoration(
                                            color: Color(0xff18BA45),
                                            borderRadius: BorderRadius.only(
                                                bottomRight:
                                                    Radius.circular(100),
                                                topRight:
                                                    Radius.circular(100))),
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        )
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 20),
              padding: EdgeInsets.symmetric(horizontal: 8),
              child: Column(
                children: [
                  Container(
                    alignment: Alignment.bottomCenter,
                    height: h - h * 0.46,
                    child: loading && user != null
                        ? Center(
                            child: CircularProgressIndicator(),
                          )
                        : ListView.builder(
                            reverse: true,
                            itemBuilder: (context, index) {
                              return Container(
                                margin: EdgeInsets.only(bottom: 24),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    Row(
                                      mainAxisAlignment: messages[index]['user']
                                                  ['id'] !=
                                              user['id']
                                          ? MainAxisAlignment.start
                                          : MainAxisAlignment.end,
                                      children: [
                                        Container(
                                          constraints: BoxConstraints(
                                              maxWidth: MediaQuery.of(context)
                                                      .size
                                                      .width /
                                                  1.5),
                                          padding: EdgeInsets.symmetric(
                                              vertical: 12, horizontal: 14),
                                          decoration: BoxDecoration(
                                              color: messages[index]['user']
                                                          ['id'] !=
                                                      user['id']
                                                  ? Color(0xff4D6CDA)
                                                  : Colors.white,
                                              border: Border.all(
                                                color: messages[index]['user']
                                                            ['id'] !=
                                                        user['id']
                                                    ? Color(0xff4D6CDA)
                                                    : Color(0xff3CD2C0),
                                              ),
                                              borderRadius: BorderRadius.only(
                                                bottomLeft: messages[index]
                                                            ['user']['id'] !=
                                                        user['id']
                                                    ? Radius.circular(0)
                                                    : Radius.circular(20),
                                                topLeft: Radius.circular(20),
                                                topRight: Radius.circular(20),
                                                bottomRight: messages[index]
                                                            ['user']['id'] !=
                                                        user['id']
                                                    ? Radius.circular(20)
                                                    : Radius.circular(0),
                                              )),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              index == messages.length - 1
                                                  ? Text(
                                                      widget.role != 2
                                                          ? ''
                                                          : 'Ваш вопрос ',
                                                      textAlign: TextAlign.left,
                                                      style: GoogleFonts.openSans(
                                                          textStyle: TextStyle(
                                                              fontSize: 14,
                                                              color: messages[index]
                                                                              [
                                                                              'user']
                                                                          [
                                                                          'id'] !=
                                                                      user['id']
                                                                  ? Colors.white
                                                                  : Color(
                                                                      0xff2DA394))),
                                                    )
                                                  : SizedBox.shrink(),
                                              Column(
                                                children: [
                                                  Text(
                                                    '${messages[index]['message']}',
                                                    style: GoogleFonts.openSans(
                                                      textStyle: TextStyle(
                                                          fontSize: 14,
                                                          fontWeight:
                                                              FontWeight.w400,
                                                          color: messages[index]
                                                                          [
                                                                          'user']
                                                                      ['id'] !=
                                                                  user['id']
                                                              ? Colors.white
                                                              : Color(
                                                                  0xff2DA394)),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      mainAxisAlignment: messages[index]['user']
                                                  ['id'] !=
                                              user['id']
                                          ? MainAxisAlignment.start
                                          : MainAxisAlignment.end,
                                      children: [
                                        Container(
                                          margin: EdgeInsets.only(top: 6),
                                          child: Text(
                                            '${messages[index]['created_at']}',
                                            style: GoogleFonts.openSans(
                                              fontSize: 12,
                                              fontWeight: FontWeight.w400,
                                            ),
                                            textAlign: TextAlign.right,
                                          ),
                                        ),
                                      ],
                                    )
                                  ],
                                ),
                              );
                            },
                            itemCount: messages.length,
                          ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                        color: Color(0xffE6E7ED),
                        borderRadius: BorderRadius.circular(20)),
                    child: TextField(
                      onSubmitted: (String value) {
                        sendMessage();
                      },
                      controller: message,
                      textInputAction: TextInputAction.go,
                      decoration: new InputDecoration(
                          border: InputBorder.none,
                          focusedBorder: InputBorder.none,
                          enabledBorder: InputBorder.none,
                          errorBorder: InputBorder.none,
                          disabledBorder: InputBorder.none,
                          contentPadding: EdgeInsets.only(
                              left: 24, bottom: 15, top: 15, right: 24),
                          hintText: translate(language, "write-message")),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
