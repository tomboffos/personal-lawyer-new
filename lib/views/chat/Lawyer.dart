import 'dart:convert';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart';
import 'package:personal_lawyer_new/views/HomePage.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'Review.dart';

class Lawyer extends StatefulWidget {
  final Map<String, dynamic> user;
  final Map<String, dynamic> order;

  const Lawyer({Key key, this.user, this.order}) : super(key: key);

  @override
  _LawyerState createState() => _LawyerState();
}

class _LawyerState extends State<Lawyer> {
  Map<String, dynamic> user;

  getLawyer() async {
    final prefs = await SharedPreferences.getInstance();
    final token = prefs.getString('token');
    final response = await get(
        Uri.parse('${apiEndpoint}user/lawyer/${widget.user['id']}'),
        headers: {"Authorization": "Bearer $token"});
    final body = jsonDecode(response.body);
    if (response.statusCode == 200) {
      setState(() {
        user = body['data'];
      });
    }
  }

  updateQuestion(String isPositive) async {
    final prefs = await SharedPreferences.getInstance();
    final token = prefs.getString('token');

    final response =
        await post(Uri.parse('${apiEndpoint}user/rating'), headers: {
      "Authorization": "Bearer $token",
      "Accept" : 'application/json'
    }, body: {
      "is_positive": isPositive,
      "lawyer_id": widget.user['id'].toString(),
      "order_id": widget.order['id'].toString()
    });
    final body = jsonDecode(response.body);
    print(body);
    if (response.statusCode == 201) {
      AwesomeDialog(
        context: context,
        borderSide: BorderSide(color: Colors.green, width: 2),
        width: 500,
        buttonsBorderRadius: BorderRadius.all(Radius.circular(2)),
        headerAnimationLoop: true,
        animType: AnimType.BOTTOMSLIDE,
        title: 'Успешно',
        desc: '${body['message']}',
        useRootNavigator: true,
        btnOkOnPress: () => {},
        showCloseIcon: false,
        dialogType: DialogType.SUCCES,
      )..show();
      await getLawyer();
    } else {
      AwesomeDialog(
        context: context,
        borderSide: BorderSide(color: Colors.green, width: 2),
        width: 500,
        buttonsBorderRadius: BorderRadius.all(Radius.circular(2)),
        headerAnimationLoop: true,
        animType: AnimType.BOTTOMSLIDE,
        title: 'Ошибка',
        desc: '${body['message']}',
        useRootNavigator: true,
        btnOkOnPress: () => {},
        showCloseIcon: false,
        dialogType: DialogType.ERROR,
      )..show();
      await getLawyer();
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    user = widget.user;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var w = MediaQuery.of(context).size.width;
    var h = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        centerTitle: false,
        elevation: 1,
        title: Text(
          'Профиль ',
          style: Theme.of(context).textTheme.headline3,
        ),
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: SingleChildScrollView(
        physics: const BouncingScrollPhysics(
            parent: AlwaysScrollableScrollPhysics()),
        child: user != null
            ? Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    height: h - h * 0.60,
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        Container(
                          decoration: BoxDecoration(
                            border: Border(
                                bottom: BorderSide(
                                    color: Theme.of(context).primaryColor)),
                            color: Colors.white,
                          ),
                          height: h - h * 0.66,
                          width: w,
                          padding: EdgeInsets.only(top: 20),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Container(
                                height: 70,
                                width: 70,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(100)),
                                child: Image(
                                  image: NetworkImage(user['avatar']),
                                  fit: BoxFit.cover,
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 10),
                                child: Text(
                                  '${user['name']}',
                                  style: GoogleFonts.openSans(
                                      textStyle: TextStyle(
                                          fontSize: 16,
                                          fontWeight: FontWeight.w600,
                                          color: Color(0xff505050))),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 5),
                                child: Text(
                                  'Юрист',
                                  style: GoogleFonts.openSans(
                                      textStyle: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.w400,
                                          color: Color(0xff626262))),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 10),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    GestureDetector(
                                      onTap: () => updateQuestion("1"),
                                      child: Container(
                                        child: Icon(
                                          Icons.thumb_up_rounded,
                                          size: 30,
                                          color: Colors.grey,
                                        ),
                                        margin: EdgeInsets.only(right: 10),
                                      ),
                                    ),
                                    Container(
                                      child: Text('${user['positive_rating']}'),
                                      margin: EdgeInsets.only(right: 10),
                                    ),
                                    Container(
                                      child: Text(
                                        '|',
                                      ),
                                      margin: EdgeInsets.only(right: 10),
                                    ),
                                    GestureDetector(
                                      onTap: () => updateQuestion("0"),
                                      child: Container(
                                        child: Icon(
                                          Icons.thumb_down_rounded,
                                          size: 30,
                                          color: Colors.grey,
                                        ),
                                        margin: EdgeInsets.only(right: 10),
                                      ),
                                    ),
                                    Text('${user['negative_rating']}'),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        // Positioned(
                        //     bottom: 0,
                        //     child: GestureDetector(
                        //       onTap: (){
                        //         Navigator.push(context, MaterialPageRoute(builder: (context)=>Review()));
                        //       },
                        //       child: Container(
                        //         padding:
                        //             EdgeInsets.symmetric(horizontal: 18, vertical: 11),
                        //         decoration: BoxDecoration(
                        //             color: Colors.white,
                        //             borderRadius: BorderRadius.circular(100),
                        //             border: Border.all(
                        //                 color: Theme.of(context).primaryColor)),
                        //         child: Text(
                        //           'Написать отзыв',
                        //           style: GoogleFonts.openSans(
                        //               color: Theme.of(context).primaryColor,
                        //               fontWeight: FontWeight.w600,
                        //               fontSize: 14),
                        //         ),
                        //       ),
                        //     ))
                      ],
                    ),
                  ),
                  // Container(
                  //   child: Column(
                  //     mainAxisAlignment: MainAxisAlignment.start,
                  //     crossAxisAlignment: CrossAxisAlignment.start,
                  //     children: [
                  //       Container(
                  //         child: Text(
                  //           'Отзывы (12)',
                  //           style: GoogleFonts.openSans(
                  //             color: Color(0xff424242),
                  //             fontSize: 16,
                  //             fontWeight: FontWeight.w600,
                  //           ),
                  //         ),
                  //         padding: EdgeInsets.only(top: 30, left: 16, right: 16),
                  //       ),
                  //       Container(
                  //         margin: EdgeInsets.only(top: 20),
                  //         height: h - h * 0.64,
                  //         child: ListView.builder(
                  //           shrinkWrap: true,
                  //           itemBuilder: (context, index) {
                  //             return Container(
                  //               padding: EdgeInsets.all(20),
                  //               margin: EdgeInsets.only(bottom: 10),
                  //               decoration: BoxDecoration(
                  //                 color: Colors.white,
                  //               ),
                  //               child: Column(
                  //                 children: [
                  //                   Row(
                  //                     children: [
                  //                       Container(
                  //                         margin: EdgeInsets.only(right: 10),
                  //                         width: 35,
                  //                         height: 35,
                  //                         decoration: BoxDecoration(
                  //                             borderRadius: BorderRadius.circular(20),
                  //                             color: Color(0xffc4c4c4)),
                  //                       ),
                  //                       Text(
                  //                         'Максим Ожидаев',
                  //                         style: GoogleFonts.openSans(
                  //                             fontWeight: FontWeight.w600,
                  //                             fontSize: 14,
                  //                             color: Color(0xff262626)),
                  //                       ),
                  //                     ],
                  //                   ),
                  //                   Container(
                  //                     margin: EdgeInsets.only(top: 10),
                  //                     child: Text(
                  //                       'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s. Lorem Ipsum is simply dummy',
                  //                       style: GoogleFonts.openSans(
                  //                           fontSize: 14,
                  //                           fontWeight: FontWeight.w400,
                  //                           color: Color(0xff4f4f4f)),
                  //                     ),
                  //                   ),
                  //                   Container(
                  //                     margin: EdgeInsets.only(top: 10),
                  //                     alignment: Alignment.bottomRight,
                  //                     child: Text(
                  //                       '2 недели назад',
                  //                       style: GoogleFonts.openSans(
                  //                           fontSize: 12,
                  //                           fontWeight: FontWeight.w400,
                  //                           color: Color(0xff747474)),
                  //                     ),
                  //                   )
                  //                 ],
                  //               ),
                  //             );
                  //           },
                  //           itemCount: 2,
                  //         ),
                  //       )
                  //     ],
                  //   ),
                  // ),
                ],
              )
            : Container(
                margin: EdgeInsets.only(top: 150),
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              ),
      ),
    );
  }
}
