import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Review extends StatefulWidget {
  final isSupport;

  const Review({Key key, this.isSupport}) : super(key: key);
  @override
  _ReviewState createState() => _ReviewState();
}

class _ReviewState extends State<Review> {
  @override
  Widget build(BuildContext context) {
    var w = MediaQuery.of(context).size.width;
    var h = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text(
          '${this.widget.isSupport == true ? 'Тех поддержка' : 'Написать отзыв'}',
          style: Theme.of(context).textTheme.headline3,
        ),
        centerTitle: false,
        iconTheme: IconThemeData(),
      ),
      body: Container(
        padding: EdgeInsets.symmetric(vertical: 16, horizontal: 16),
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.only(top: 20),
              padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(20),
                border: Border.all(color: Color(0xffDCDCDC), width: 1),
              ),
              child: TextField(
                maxLines: 7,
                decoration: InputDecoration(
                  hintText: '${this.widget.isSupport == true ? 'Что вас беспокоит....' :'Напишите о нас ....'}',
                  border: InputBorder.none,
                  focusedBorder: InputBorder.none,
                  enabledBorder: InputBorder.none,
                  errorBorder: InputBorder.none,
                  disabledBorder: InputBorder.none,
                ),
              ),
            ),
            Container(
              width: w - 32,
              margin: EdgeInsets.only(top: 24),

              child: Container(
                padding: EdgeInsets.symmetric(
                    vertical: 15
                ),
                decoration:
                    BoxDecoration(
                      color: Theme.of(context).primaryColor,
                      borderRadius: BorderRadius.circular(25)
                    ),
                child: Text(
                  'Отправить',
                  style: GoogleFonts.openSans(
                    fontSize: 16,
                    fontWeight: FontWeight.w600,
                    color: Colors.white,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
