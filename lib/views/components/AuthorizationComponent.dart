import 'dart:convert';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart';
import 'package:personal_lawyer_new/views/authorization/PhoneConfirmation.dart';
import 'package:personal_lawyer_new/views/authorization/PinCode.dart';
import 'package:url_launcher/url_launcher.dart';

import '../HomePage.dart';

class AuthorizationComponent extends StatefulWidget {
  final isLogin;

  const AuthorizationComponent({Key key, this.isLogin}) : super(key: key);

  @override
  _AuthorizationComponentState createState() => _AuthorizationComponentState();
}

class _AuthorizationComponentState extends State<AuthorizationComponent> {
  TextEditingController phoneController =
      new MaskedTextController(mask: '+0 (000) 000-00-00', text: '+7');
  TextEditingController nameController = new TextEditingController();
  final FocusNode _nodeText1 = FocusNode();

  bool loading = false;
  bool accepted = false;

  register(Map<String, dynamic> form) async {
    setState(() {
      loading = true;
    });
    if(!accepted){
      setState(() {
        loading = false;

      });
      AwesomeDialog(
        context: context,
        borderSide: BorderSide(color: Colors.green, width: 2),
        width: 500,
        buttonsBorderRadius: BorderRadius.all(Radius.circular(2)),
        headerAnimationLoop: true,
        animType: AnimType.BOTTOMSLIDE,
        title: 'Ошибка',
        desc: 'Примите политику конфиденциальности',
        showCloseIcon: false,
        dialogType: DialogType.ERROR,

      )
        ..show();
      return 0;
    }
    final response = await post(Uri.parse('${apiEndpoint}auth/register'),
        headers: {"Accept": "application/json"}, body: form);
    final body = jsonDecode(response.body);
    print(form);
    print(body);
    if (body['data'] != null) {
      setState(() {
        loading = true;
      });
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => PhoneConfirmation(
                    user: body['data'],
                    isLogin: 0,
                  )));
    }else{
      setState(() {
        loading = false;

      });
      AwesomeDialog(
        context: context,
        borderSide: BorderSide(color: Colors.green, width: 2),
        width: 500,
        buttonsBorderRadius: BorderRadius.all(Radius.circular(2)),
        headerAnimationLoop: true,
        animType: AnimType.BOTTOMSLIDE,
        title: 'Ошибка',
        desc: '${body['errors']['phone'][0]}',
        showCloseIcon: false,
        dialogType: DialogType.ERROR,

      )
        ..show();
    }
  }

  login(Map<String, dynamic> form) async {
    setState(() {
      loading = true;
    });
    final response = await post(Uri.parse('${apiEndpoint}auth/login'),
        headers: {"Accept": "application/json"}, body: form);

    final body = jsonDecode(response.body);

    print(body);
    if (body['data'] != null) {
      setState(() {
        loading = false;
      });
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => PhoneConfirmation(
                    user: body['data'],
                    isLogin: 1,
                  )));
    }
  }

  @override
  Widget build(BuildContext context) {
    var w = MediaQuery.of(context).size.width;
    var h = MediaQuery.of(context).size.height;
    return GestureDetector(
      onTap: () => FocusScope.of(context).requestFocus(new FocusNode()),
      child: Container(
        height: 500,
        padding: EdgeInsets.symmetric(vertical: 44, horizontal: 16),
        child: Column(
          children: [
            TextField(
              keyboardType: TextInputType.phone,
              controller: phoneController,
              focusNode: _nodeText1,
              textInputAction: TextInputAction.send,
              decoration: InputDecoration(
                  labelText: 'Телефон',
                  floatingLabelBehavior: FloatingLabelBehavior.always,
                  labelStyle: TextStyle(
                      color: Theme.of(context).primaryColor, fontSize: 20),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(50),
                    borderSide:
                        BorderSide(color: Theme.of(context).primaryColor),
                  ),
                  border: new OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(100),
                    borderSide:
                        new BorderSide(color: Theme.of(context).primaryColor),
                  )),
            ),
            this.widget.isLogin == 0
                ? Container(
                    padding: EdgeInsets.only(top: 22),
                    child: TextField(
                      controller: nameController,
                      decoration: InputDecoration(
                          labelText: 'Имя',
                          labelStyle: TextStyle(
                              color: Theme.of(context).primaryColor,
                              fontSize: 20),
                          floatingLabelBehavior: FloatingLabelBehavior.always,
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(50),
                            borderSide: BorderSide(
                                color: Theme.of(context).primaryColor),
                          ),
                          border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(100),
                            borderSide: new BorderSide(
                                color: Theme.of(context).primaryColor),
                          )),
                    ),
                  )
                : SizedBox.shrink(),
            this.widget.isLogin == 0
                ? Container(
                    child: CheckboxListTile(
                    value: accepted,
                    onChanged: (value) {
                      setState(() {
                        accepted = !accepted;
                      });
                    },
                    title: GestureDetector(
                      onTap: ()=>launch('https://personal-lawyer.a-lux.dev/storage/documents/agreement.pdf'),
                        child: Text(
                      'Договор оферты',
                      style: GoogleFonts.openSans(
                          decoration: TextDecoration.underline),
                    )),
                  ))
                : SizedBox.shrink(),
            Spacer(),
            GestureDetector(
              onTap: () {
                if (loading == false) {
                  if (this.widget.isLogin == 1) {
                    login({"phone": phoneController.text});
                  } else {
                    register({
                      "phone": phoneController.text,
                      "name": nameController.text,
                    });
                  }
                }
              },
              child: Container(
                width: w - 16,
                alignment: Alignment.center,
                padding: EdgeInsets.symmetric(vertical: 15),
                decoration: BoxDecoration(
                  color: Theme.of(context).primaryColor,
                  borderRadius: BorderRadius.circular(25),
                ),
                child: loading
                    ? Center(
                        child: CircularProgressIndicator(),
                      )
                    : Text(
                        '${this.widget.isLogin == 1 ? 'Войти' : 'Зарегистрироваться'}',
                        style: GoogleFonts.openSans(
                          textStyle: TextStyle(
                              color: Colors.white,
                              fontSize: 20,
                              fontWeight: FontWeight.w600),
                        )),
              ),
            )
          ],
        ),
      ),
    );
  }
}
