import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:personal_lawyer_new/views/HomePage.dart';
import 'package:personal_lawyer_new/views/payment/PayDocument.dart';
import 'package:personal_lawyer_new/views/profile/Card.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DocumentPurchase extends StatefulWidget {
  final mainDocument;
  final category;
  final lang;

  const DocumentPurchase({Key key, this.mainDocument, this.category, this.lang})
      : super(key: key);

  @override
  _DocumentPurchaseState createState() => _DocumentPurchaseState();
}

class _DocumentPurchaseState extends State<DocumentPurchase> {
  bool loading = false;
  List<dynamic> documents = [];
  String language;
  void getLocale()async{
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      language = prefs.getString('lang');
    });
    print(language);
  }

  loadDocuments() async {
    setState(() {
      loading = true;
    });
    final response =
        await get('${apiEndpoint}documents/${this.widget.category['id']}?lang=${widget.lang}');
    if (response.statusCode == 200) {
      final body = jsonDecode(response.body);
      setState(() {
        documents = body['data'];
        loading = false;
      });
    }
  }

  payDocument(Map<String, dynamic> document) async {
    setState(() {
      loading = true;
    });
    final prefs = await SharedPreferences.getInstance();
    final token = prefs.getString('token');
    final response = await post(Uri.parse('${apiEndpoint}documents'), body: {
      "document_id": document['id'].toString()
    }, headers: {
      "Authorization": "Bearer $token",
      "Accept": "application/json"
    });

    final body = jsonDecode(response.body);
    print(body);
    if (response.statusCode == 200) {
      setState(() {
        loading = false;
      });
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => PayDocument(
                    data: body['payment'],
                  )));
    }else if(response.statusCode == 404){
      setState(() {
        loading = false;
      });
      pushNewScreen(context, screen: CardsPage(),withNavBar: false);
    }
  }

  getUserData() async {
    final prefs = await SharedPreferences.getInstance();
    final token = prefs.getString('token');
    final response = await get(Uri.parse('${apiEndpoint}user'),
        headers: {"Authorization": "Bearer $token"});
    print(response.body);
  }

  @override
  void initState() {
    // TODO: implement initState
    getLocale();
    if (this.widget.category != null){
      loadDocuments();

    }

    getUserData();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var w = MediaQuery.of(context).size.width;
    var h = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: AppBar(
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
        title: Text(
          '${this.widget.mainDocument == true ? 'Мои документы' : '${this.widget.category['name']}'}',
          style: Theme.of(context).textTheme.headline3,
        ),
        centerTitle: false,
      ),
      body: SingleChildScrollView(
        child: Container(
            padding: EdgeInsets.only(top: 20, left: 16, right: 16),
            child: loading
                ? Center(
                    child: CircularProgressIndicator(),
                  )
                : ListView.builder(
                    physics: ScrollPhysics(),
                    shrinkWrap: true,
                    itemBuilder: (context, index) {
                      return GestureDetector(
                        onTap: () => payDocument(documents[index]),
                        child: Container(
                          margin: EdgeInsets.only(bottom: 5),
                          padding: EdgeInsets.symmetric(
                              vertical: 20, horizontal: 20),
                          decoration: BoxDecoration(
                            border: Border.all(
                              color: Theme.of(context).primaryColor,
                            ),
                            borderRadius: BorderRadius.circular(15),
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                width: w - w * 0.3,
                                child: Text(
                                  '${documents[index]['name']}',
                                  style: GoogleFonts.openSans(
                                      fontSize: 16,
                                      fontWeight: FontWeight.w400,
                                      color: Theme.of(context).primaryColor),
                                ),
                              ),
                              Container(
                                width: 42,
                                height: 42,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(100),
                                    color: Color(0xff2A9D8F)),
                                child: Icon(
                                  this.widget.mainDocument != true
                                      ? Icons.shopping_cart
                                      : Icons.download_rounded,
                                  color: Colors.white,
                                ),
                              )
                            ],
                          ),
                        ),
                      );
                    },
                    itemCount: documents.length,
                  )),
      ),
    );
  }
}
