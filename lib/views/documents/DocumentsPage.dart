import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart';
import 'package:personal_lawyer_new/views/HomePage.dart';
import 'package:personal_lawyer_new/views/documents/DocumentPurchase.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../language_service.dart';

class DocumentPage extends StatefulWidget {
  final mainDocuments;

  const DocumentPage({Key key, this.mainDocuments}) : super(key: key);

  @override
  _DocumentPageState createState() => _DocumentPageState();
}

class _DocumentPageState extends State<DocumentPage> {
  List<dynamic> categories = [];
  bool loading = false;

  loadCategories() async {
    setState(() {
      loading = true;
    });
    final local = (await SharedPreferences.getInstance()).get('lang');
    final response = await get(Uri.parse('${apiEndpoint}documents/categories?local=$local'),
        headers: {"Accept": "application/json"});
    print(response.body);

    if (response.statusCode == 200) {
      final body = jsonDecode(response.body);
      setState(() {
        loading = false;
        categories = body['data'];
      });
    }
  }

  String language;

  void getLocale() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      language = prefs.getString('lang');
    });
    print(language);
  }

  @override
  void initState() {
    // TODO: implement initState
    getLocale();
    loadCategories();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var w = MediaQuery.of(context).size.width;
    var h = MediaQuery.of(context).size.height;

    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text(
          '${this.widget.mainDocuments == true ? 'Мои документы' : translate(language, 'document-template')}',
          style: Theme.of(context).textTheme.headline3,
        ),
        centerTitle: false,
        iconTheme: IconThemeData(),
      ),
      body: SingleChildScrollView(
        physics: NeverScrollableScrollPhysics(),
        child: Container(
            padding: EdgeInsets.only(top: 20, left: 16, right: 16),
            child: loading
                ? Center(
                    child: CircularProgressIndicator(),
                  )
                : ListView.builder(
                    physics: ScrollPhysics(),
                    shrinkWrap: true,
                    itemBuilder: (context, index) {
                      return GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => DocumentPurchase(
                                        mainDocument: this.widget.mainDocuments,
                                        category: categories[index],
                                        lang: language,
                                      )));
                        },
                        child: Container(
                          margin: EdgeInsets.only(bottom: 5),
                          padding: EdgeInsets.symmetric(
                              vertical: 20, horizontal: 20),
                          decoration: BoxDecoration(
                            border: Border.all(
                              color: Theme.of(context).primaryColor,
                            ),
                            borderRadius: BorderRadius.circular(15),
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                width: w - w * 0.3,
                                child: Text(
                                  '${categories[index]['name']}',
                                  style: GoogleFonts.openSans(
                                      fontSize: 16,
                                      fontWeight: FontWeight.w400,
                                      color: Theme.of(context).primaryColor),
                                ),
                              ),
                              Icon(
                                Icons.chevron_right,
                                color: Theme.of(context).primaryColor,
                              )
                            ],
                          ),
                        ),
                      );
                    },
                    itemCount: categories.length,
                  )),
      ),
    );
  }
}
