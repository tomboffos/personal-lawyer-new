import 'dart:convert';
import 'dart:io';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart';
import 'package:personal_lawyer_new/views/documents/DocumentsPage.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../language_service.dart';
import '../Consultation.dart';
import '../HomePage.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

String iosVersion = '1.0.20';

class _HomeScreenState extends State<HomeScreen> {
  String language;

  void changeLocale(lang) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString('lang', lang);
    setState(() {
      language = lang;
    });
  }

  void checkVersions() async {
    final response = await get(Uri.parse('${apiEndpoint}versions'));

    final body = jsonDecode(response.body);
    if (iosVersion != body['ios']) {
      AwesomeDialog(
          context: context,
          borderSide: BorderSide(color: Colors.green, width: 2),
          width: 500,
          buttonsBorderRadius: BorderRadius.all(Radius.circular(2)),
          headerAnimationLoop: true,
          animType: AnimType.BOTTOMSLIDE,
          title: 'Ошибка',
          desc: 'Обновите приложение',
          showCloseIcon: false,
          dialogType: DialogType.ERROR,
          dismissOnTouchOutside: false,
          useRootNavigator: true,
          btnOkOnPress: () => launch(Platform.isIOS
              ? 'https://apps.apple.com/ru/app/%D0%B2%D0%B0%D1%88-%D0%BB%D0%B8%D1%87%D0%BD%D1%8B%D0%B9-%D0%B0%D0%B4%D0%B2%D0%BE%D0%BA%D0%B0%D1%82/id1499337941'
              : 'https://play.google.com/store/apps/details?id=kz.itgroup.personallawyer&hl=ru&gl=US'))
        ..show();
    }
  }

  void getLocale() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      language = prefs.getString('lang');
    });
    print(language);
  }

  @override
  void initState() {
    // TODO: implement initState
    getLocale();
    checkVersions();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var w = MediaQuery.of(context).size.width;
    var h = MediaQuery.of(context).size.height;
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              height: h - h * 0.75,
              width: w,
              decoration: BoxDecoration(
                  color: Theme.of(context).primaryColor,
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(50),
                      bottomRight: Radius.circular(50))),
              child: Container(
                margin: EdgeInsets.symmetric(vertical: 60, horizontal: 16),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          translate(language, 'personal-lawyer'),
                          style: Theme.of(context).textTheme.headline4,
                        ),
                        Row(
                          children: [
                            GestureDetector(
                              onTap: () => changeLocale('ru'),
                              child: Container(
                                padding: EdgeInsets.all(4),
                                margin: EdgeInsets.symmetric(horizontal: 4),
                                decoration: BoxDecoration(
                                    border: Border.all(
                                      color: language == 'ru'
                                          ? Colors.white
                                          : Theme.of(context).primaryColor,
                                    ),
                                    borderRadius: BorderRadius.circular(10)),
                                child: Text(
                                  'Рус',
                                  style: GoogleFonts.openSans(
                                      textStyle: TextStyle(
                                          fontSize: 14,
                                          color: language == 'ru'
                                              ? Colors.white
                                              : Color(0xff013B93),
                                          fontWeight: FontWeight.w600)),
                                ),
                              ),
                            ),
                            GestureDetector(
                              onTap: () => changeLocale('kz'),
                              child: Container(
                                padding: EdgeInsets.all(4),
                                margin: EdgeInsets.symmetric(horizontal: 4),
                                decoration: BoxDecoration(
                                    border: Border.all(
                                      color: language == 'kz'
                                          ? Colors.white
                                          : Theme.of(context).primaryColor,
                                    ),
                                    borderRadius: BorderRadius.circular(10)),
                                child: Text(
                                  'Каз',
                                  style: GoogleFonts.openSans(
                                      textStyle: TextStyle(
                                          fontSize: 14,
                                          color: language == 'kz'
                                              ? Colors.white
                                              : Color(0xff013B93),
                                          fontWeight: FontWeight.w600)),
                                ),
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                    GestureDetector(
                      onTap: () => launch('https://go.2gis.com/n4ygm'),
                      child: Container(
                        margin: EdgeInsets.only(top: 10),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(50)),
                        child: ListTile(
                          leading: Icon(
                            Icons.location_on,
                            color: Theme.of(context).primaryColor,
                          ),
                          title: Text(
                            'Шевченко 165Б, 301 офис, Алматы',
                            style: GoogleFonts.openSans(
                                textStyle: TextStyle(
                                    fontSize: 15, fontWeight: FontWeight.w400)),
                          ),
                        ),
                      ),
                    ),
                    // GestureDetector(
                    //   onTap: () => launch('https://personal-lawyer.kz'),
                    //   child: Container(
                    //     margin: EdgeInsets.only(top: 15),
                    //     decoration: BoxDecoration(
                    //         color: Colors.white,
                    //         borderRadius: BorderRadius.circular(50)),
                    //     child: ListTile(
                    //       leading: Icon(
                    //         Icons.web,
                    //         color: Theme
                    //             .of(context)
                    //             .primaryColor,
                    //       ),
                    //       title: Text(
                    //         'www.personal-lawyer.kz',
                    //         style: GoogleFonts.openSans(
                    //             textStyle: TextStyle(
                    //                 fontSize: 15, fontWeight: FontWeight.w400)),
                    //       ),
                    //     ),
                    //   ),
                    // )
                  ],
                ),
              ),
            ),
            Container(
              width: w,
              padding: EdgeInsets.symmetric(vertical: 20, horizontal: 16),
              child: Column(
                children: [
                  GestureDetector(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ConsultationQuestion()));
                    },
                    child: Container(
                      width: w - 32,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(15),
                          image: DecorationImage(
                            image: AssetImage('assets/image_background1.png'),
                            fit: BoxFit.cover,
                          )),
                      padding: EdgeInsets.only(top: 150, bottom: 40, right: 83),
                      child: Container(
                        padding:
                            EdgeInsets.symmetric(vertical: 10, horizontal: 12),
                        decoration: BoxDecoration(
                            color: Color.fromRGBO(0, 121, 194, 0.5),
                            borderRadius: BorderRadius.only(
                                bottomRight: Radius.circular(15),
                                topRight: Radius.circular(15))),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              translate(language, 'get-consultation'),
                              style: GoogleFonts.openSans(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w700,
                                  fontSize: 16),
                              textAlign: TextAlign.left,
                            ),
                            Text(
                              '3000 тенге',
                              style: GoogleFonts.openSans(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w400,
                                  fontSize: 16),
                              textAlign: TextAlign.left,
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => DocumentPage()));
                    },
                    child: Container(
                      margin: EdgeInsets.only(top: 15),
                      width: w - 32,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(15),
                          image: DecorationImage(
                            image: AssetImage('assets/image_background2.png'),
                            fit: BoxFit.cover,
                          )),
                      padding: EdgeInsets.only(top: 150, bottom: 30, right: 83),
                      child: Container(
                        padding:
                            EdgeInsets.symmetric(vertical: 10, horizontal: 12),
                        decoration: BoxDecoration(
                            color: Color.fromRGBO(0, 121, 194, 0.5),
                            borderRadius: BorderRadius.only(
                                bottomRight: Radius.circular(15),
                                topRight: Radius.circular(15))),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              translate(language, 'document-template'),
                              style: GoogleFonts.openSans(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w700,
                                  fontSize: 16),
                              textAlign: TextAlign.left,
                            ),
                            Text(
                              '1000 тенге',
                              style: GoogleFonts.openSans(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w400,
                                  fontSize: 16),
                              textAlign: TextAlign.left,
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 40),
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: RaisedButton(
                      color: Theme.of(context).primaryColor,
                      onPressed: () =>
                          launch('mailto:personallawyer07@gmail.com'),
                      child: Container(
                        margin:
                            EdgeInsets.symmetric(horizontal: 40, vertical: 15),
                        child: Text(
                          translate(language, "technical-support"),
                          style: GoogleFonts.openSans(
                              fontSize: 14,
                              fontWeight: FontWeight.w700,
                              color: Colors.white),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
