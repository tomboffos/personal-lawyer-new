import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_countdown_timer/current_remaining_time.dart';
import 'package:flutter_countdown_timer/flutter_countdown_timer.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart';
import 'package:personal_lawyer_new/views/HomePage.dart';
import 'package:personal_lawyer_new/views/chat/ChatPage.dart';
import 'package:personal_lawyer_new/views/documents/DocumentPurchase.dart';
import 'package:pusher_client/pusher_client.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:awesome_dialog/awesome_dialog.dart';

import '../../language_service.dart';

class Messages extends StatefulWidget {
  final user;

  const Messages({Key key, this.user}) : super(key: key);
  @override
  _MessagesState createState() => _MessagesState();
}

class _MessagesState extends State<Messages> {
  List<dynamic> questions = [];
  bool loading = false;
  Map<String, dynamic> user;
  fetchUser() async {
    setState(() {
      loading = true;
    });
    final token = (await SharedPreferences.getInstance()).get('token');
    final response = await get(Uri.parse('${apiEndpoint}user'),
        headers: {"Authorization": "Bearer $token"});
    setState(() {
      loading = false;
      user = jsonDecode(response.body)['data'];
      fetchQuestions(true);
      initListenerPusher();
      getLocale();
    });
  }

  String language;
  initListenerPusher() {
    PusherClient pusher =
        PusherClient('71b7e222b90fdffa56e6', PusherOptions(cluster: 'ap2'));

    pusher.connect();
    Channel channel = pusher.subscribe("App.Chats");

    channel.bind('chat-created', (PusherEvent event) => fetchQuestions(true));
    pusher.onConnectionStateChange((state) {
      print(
          "previousState: ${state.previousState},\n currentState: ${state.currentState}");
    });
  }

  getQuestion(order) async {
    final token = (await SharedPreferences.getInstance()).get('token');

    final response =
        await get('${apiEndpoint}user/lawyers/orders/${order['id']}', headers: {
      "Authorization": "Bearer $token",
      "Accept": "application/json"
    });
    print(response.statusCode);
    if (response.statusCode == 200)
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => ChatPage(
                    role: user['role_id'],
                    question: jsonDecode(response.body)['data'],
                  )));
    print(response.body);
  }

  void getLocale() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      language = prefs.getString('lang');
    });
    print(language);
  }

  fetchQuestions(withoutLoading) async {
    print(user['role_id'] == 3);
    if (user['role_id'] == 4)
      await fetchOrdersQuestion(withoutLoading);
    else if (user['role_id'] == 2)
      await fetchUserQuestions(withoutLoading);
    else
      await fetchLawyerQuestions(withoutLoading);
    // if(user['role_id']) == "3");
  }

  fetchLawyerQuestions(withoutLoading) async {
    final token = (await SharedPreferences.getInstance()).get('token');
    final response = await get(Uri.parse('${apiEndpoint}user/lawyers/orders'),
        headers: {
          "Authorization": "Bearer $token",
          "Accept": "application/json"
        });
    print(response.body);
    if (response.statusCode == 200) {
      final body = jsonDecode(response.body);
      print(body);
      setState(() {
        loading = false;
        questions = body['data'];
      });
    }
  }

  fetchUserQuestions(withoutLoading) async {
    final prefs = await SharedPreferences.getInstance();
    final token = prefs.getString('token');
    if (!withoutLoading) {
      setState(() {
        loading = true;
      });
    }
    final response = await get(Uri.parse('${apiEndpoint}user/questions'),
        headers: {"Authorization": "Bearer $token"});

    print(response.body);
    if (response.statusCode == 200) {
      final body = jsonDecode(response.body);
      print(body);
      setState(() {
        loading = false;
        questions = body['data'];
      });
    }
  }

  fetchOrdersQuestion(withoutLoading) async {
    final token = (await SharedPreferences.getInstance()).get('token');
    final response = await get(Uri.parse('${apiEndpoint}user/counsel/orders'),
        headers: {"Authorization": "Bearer $token"});
    if (!withoutLoading)
      setState(() {
        loading = true;
      });
    setState(() {
      loading = false;
      questions = jsonDecode(response.body)['data'];
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    fetchUser();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var w = MediaQuery.of(context).size.width;
    var h = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text(
          'Сообщения',
          style: Theme.of(context).textTheme.headline3,
        ),
        centerTitle: false,
        iconTheme: IconThemeData(),
      ),
      body: RefreshIndicator(
        onRefresh: () => fetchUser(),
        child: Container(
            height: h,
            width: w,
            padding: EdgeInsets.only(top: 20, left: 16, right: 16),
            child: loading
                ? Center(child: CircularProgressIndicator())
                : ListView.builder(
                    itemBuilder: (context, index) {
                      if (questions.length == 0)
                        return Container(
                          margin: EdgeInsets.only(
                              top: MediaQuery.of(context).size.height / 3),
                          child: Center(
                            child: Text('Пока что нет консультации'),
                          ),
                        );
                      return GestureDetector(
                        onTap: () async {
                          print(questions[index]['lawyer']);
                          if ((questions[index]['lawyer'] == null &&
                                  user['role_id'] == 3) ||
                              (questions[index]['lawyer'] != null &&
                                  user['role_id'] == 1))
                            await getQuestion(questions[index]);
                          else if (questions[index]['lawyer'] != null &&
                              user['role_id'] == 2)
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ChatPage(
                                          role: user['role_id'],
                                          question: questions[index],
                                        )));
                          else if ((questions[index]['lawyer'] != null &&
                                  user['role_id'] == 3) ||
                              (questions[index]['lawyer'] != null &&
                                  user['role_id'] == 1))
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ChatPage(
                                          role: user['role_id'],
                                          question: questions[index],
                                        )));
                          else if (questions[index]['lawyer'] != null ||
                              user['role_id'] == 4)
                            getCounselQuestion(questions[index]);
                          else
                            AwesomeDialog(
                              context: context,
                              borderSide:
                                  BorderSide(color: Colors.green, width: 2),
                              width: 500,
                              buttonsBorderRadius:
                                  BorderRadius.all(Radius.circular(2)),
                              headerAnimationLoop: true,
                              animType: AnimType.BOTTOMSLIDE,
                              title: 'Просим прощения',
                              desc: 'Пока что ваша консультация не началась',
                              useRootNavigator: true,
                              btnOkOnPress: () => {},
                              showCloseIcon: false,
                              dialogType: DialogType.ERROR,
                            )..show();
                        },
                        child: Container(
                          margin: EdgeInsets.only(bottom: 5),
                          padding: EdgeInsets.symmetric(
                              vertical: 20, horizontal: 20),
                          decoration: BoxDecoration(
                            border: Border.all(
                              color: Theme.of(context).primaryColor,
                            ),
                            borderRadius: BorderRadius.circular(15),
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    width: w - w * 0.3,
                                    child: Text(
                                      '${translate(language, "order")} ${questions[index]['id']}. ${translate(language, "question")}: ${questions[index]['question_title']}',
                                      style: GoogleFonts.openSans(
                                          fontSize: 16,
                                          fontWeight: FontWeight.w700,
                                          color:
                                              Theme.of(context).primaryColor),
                                    ),
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Container(
                                        margin: EdgeInsets.only(top: 5),
                                        child: Text(
                                          '${questions[index]['order_status']['name']}',
                                          style: GoogleFonts.openSans(
                                              fontSize: 12,
                                              fontWeight: FontWeight.w400,
                                              color: Theme.of(context)
                                                  .primaryColor),
                                        ),
                                      ),
                                      questions[index]['order_status']['id'] ==
                                              10 
                                          ? Container(
                                              margin: EdgeInsets.only(
                                                  left: 10, top: 5),
                                              padding: EdgeInsets.symmetric(
                                                  vertical: 5, horizontal: 10),
                                              decoration: BoxDecoration(
                                                  color: Color(0xff1F7E8B),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          15)),
                                              child: CountdownTimer(
                                                onEnd: () {
                                                  fetchQuestions(false);
                                                },
                                                endTime: DateTime.parse(
                                                            questions[index]
                                                                ['end_time'])
                                                        .millisecondsSinceEpoch +
                                                    1000 * 30,
                                                widgetBuilder: (_,
                                                    CurrentRemainingTime time) {
                                                  return Text(
                                                    '${time.hours != null ? '${time.hours}ч:' : ''}${time.min != null ? '${time.min}м:' : ''}${time.sec}с',
                                                    style: GoogleFonts.openSans(
                                                        fontSize: 12,
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        color: Colors.white),
                                                  );
                                                },
                                              ),
                                            )
                                          : SizedBox.shrink(),
                                    ],
                                  ),
                                ],
                              ),
                              user['role_id'] == 2
                                  ? questions[index]['new_messages'] > 0
                                      ? Container(
                                          padding: EdgeInsets.symmetric(
                                              vertical: 4, horizontal: 10),
                                          decoration: BoxDecoration(
                                              color: Color(0xff0079C2),
                                              borderRadius:
                                                  BorderRadius.circular(100)),
                                          child: Text(
                                            '${questions[index]['new_messages']}',
                                            style: GoogleFonts.openSans(
                                                textStyle: TextStyle(
                                                    fontSize: 14,
                                                    fontWeight: FontWeight.w600,
                                                    color: Colors.white)),
                                          ),
                                        )
                                      : SizedBox.shrink()
                                  : questions[index]['new_lawyer_messages'] > 0
                                      ? Container(
                                          padding: EdgeInsets.symmetric(
                                              vertical: 4, horizontal: 10),
                                          decoration: BoxDecoration(
                                              color: Color(0xff0079C2),
                                              borderRadius:
                                                  BorderRadius.circular(100)),
                                          child: Text(
                                            '${questions[index]['new_lawyer_messages']}',
                                            style: GoogleFonts.openSans(
                                                textStyle: TextStyle(
                                                    fontSize: 14,
                                                    fontWeight: FontWeight.w600,
                                                    color: Colors.white)),
                                          ),
                                        )
                                      : SizedBox.shrink()
                            ],
                          ),
                        ),
                      );
                    },
                    itemCount: questions.length == 0 ? 1 : questions.length,
                  )),
      ),
    );
  }

  void getCounselQuestion(question) async {
    final token = (await SharedPreferences.getInstance()).get('token');
    final response = await get(
        Uri.parse('${apiEndpoint}user/counsel/orders/${question['id']}'),
        headers: {
          "Authorization": "Bearer $token",
          "Accept": "application/json"
        });
    print(response.body);
    print(response.statusCode);
    if (response.statusCode == 200)
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => ChatPage(
                    role: user['role_id'],
                    question: jsonDecode(response.body)['data'],
                  )));
  }
}
