import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

class PayDocument extends StatefulWidget {
  final data;
  const PayDocument({Key key, this.data}) : super(key: key);

  @override
  _PayDocumentState createState() => _PayDocumentState();
}

class _PayDocumentState extends State<PayDocument> {
  final flutterWebViewPlugin = FlutterWebviewPlugin();
  bool isEnded = false;
  AwesomeDialog dialog;
  urlChangeHandler() {
    flutterWebViewPlugin.onUrlChanged.listen((String url) {
      print(url);
      if(url.contains('https://personal-lawyer.a-lux.dev/payment/success')){
        setState(() {
          isEnded = true;
        });
        AwesomeDialog(
          context: context,
          borderSide: BorderSide(color: Colors.green, width: 2),
          width: 500,
          buttonsBorderRadius: BorderRadius.all(Radius.circular(2)),
          headerAnimationLoop: true,
          animType: AnimType.BOTTOMSLIDE,
          title: 'Успешно',
          desc: 'Оплата прошла успешна',
          showCloseIcon: false,
          dialogType: DialogType.SUCCES,
          btnOkOnPress: (){
            Navigator.pop(context);
          },
          useRootNavigator: true

        )..show();




      }
    });
  }
  @override
  void initState() {
    // TODO: implement initState
    urlChangeHandler();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: AppBar(
        backgroundColor: Colors.white,
        centerTitle: true,
        elevation: 1,
        title: Text(
          'Мои карты',
          style: Theme.of(context).textTheme.headline3,
        ),
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: isEnded ? Container() : WebviewScaffold(
        url: new Uri.dataFromString(this.widget.data,mimeType: 'text/html').toString()
      ),
    );
  }
}
