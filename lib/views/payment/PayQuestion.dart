import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:personal_lawyer_new/views/main_screens/Messages.dart';
class PayQuestion extends StatefulWidget {
  final data;
  const PayQuestion({Key key, this.data}) : super(key: key);

  @override
  _PayQuestionState createState() => _PayQuestionState();
}

class _PayQuestionState extends State<PayQuestion> {


  final flutterWebViewPlugin = FlutterWebviewPlugin();
  bool isEnded = false;
  urlChangeHandler() {
    flutterWebViewPlugin.onUrlChanged.listen((String url) {
      print(url);
      if(url.contains('https://personal-lawyer.a-lux.dev/payment/success')){
        setState(() {
          isEnded = true;
        });
        AwesomeDialog(
          context: context,
          borderSide: BorderSide(color: Colors.green, width: 2),
          width: 500,
          buttonsBorderRadius: BorderRadius.all(Radius.circular(2)),
          headerAnimationLoop: true,
          animType: AnimType.BOTTOMSLIDE,
          title: 'Успешно',
          desc: 'Спасибо за обращение ответ от юриста будет предоставлен в течении часа',
          showCloseIcon: false,
          useRootNavigator: true,
          dialogType: DialogType.SUCCES,
          btnOkOnPress: (){
            Navigator.push(context, MaterialPageRoute(builder: (context)=>Messages()));
          }

        )..show();

      }
    });
  }


  @override
  void initState() {
    // TODO: implement initState
    urlChangeHandler();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: AppBar(
        backgroundColor: Colors.white,
        centerTitle: true,
        elevation: 1,
        title: Text(
          'Мои карты',
          style: Theme.of(context).textTheme.headline3,
        ),
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: isEnded ? Container() : WebviewScaffold(
          url: new Uri.dataFromString(this.widget.data,mimeType: 'text/html').toString()
      ),
    );
  }
}
