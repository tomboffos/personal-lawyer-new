import 'dart:convert';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:personal_lawyer_new/views/HomePage.dart';
import 'package:personal_lawyer_new/views/payment/LinkCard.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

import '../../language_service.dart';

class CardsPage extends StatefulWidget {
  @override
  _CardsPageState createState() => _CardsPageState();
}

Widget _floatingCollapsed() {
  return Container(
    decoration: BoxDecoration(
      color: Colors.blueGrey,
      borderRadius: BorderRadius.only(
          topLeft: Radius.circular(24.0), topRight: Radius.circular(24.0)),
    ),
    margin: const EdgeInsets.fromLTRB(24.0, 24.0, 24.0, 0.0),
    child: Center(
      child: Text(
        "This is the collapsed Widget",
        style: TextStyle(color: Colors.white),
      ),
    ),
  );
}

Widget _floatingPanel() {
  return Container(
    decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(24.0)),
        boxShadow: [
          BoxShadow(
            blurRadius: 20.0,
            color: Colors.grey,
          ),
        ]),
    margin: const EdgeInsets.all(24.0),
    child: Center(
      child: Text("This is the SlidingUpPanel when open"),
    ),
  );
}

class _CardsPageState extends State<CardsPage> {
  bool showSetting = false;
  bool loading = false;
  bool buttonLoading = false;
  List<dynamic> cards = [];
  List<dynamic> cardWidgets = [];
  Map<String, dynamic> card;

  String language;
  void getLocale() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      language = prefs.getString('lang');
    });
    print(language);
  }

  fetchCards() async {
    setState(() {
      loading = true;
    });
    final prefs = await SharedPreferences.getInstance();
    final token = prefs.getString('token');
    final response = await get(Uri.parse('${apiEndpoint}card'),
        headers: {"Authorization": "Bearer $token"});

    if (response.statusCode == 200) {
      final body = jsonDecode(response.body);
      print(body);
      setState(() {
        loading = false;
        cards = body['data'];
      });
    }
  }

  chooseCard(card) async {
    final prefs = await SharedPreferences.getInstance();
    final token = prefs.getString('token');
    setState(() {
      showSetting = false;
    });
    final response = await post(
        Uri.parse('${apiEndpoint}card/main/${card['id']}'),
        headers: {"Authorization": "Bearer $token"});

    if (response.statusCode == 200) {
      final body = jsonDecode(response.body);
      await fetchCards();
    }
  }

  deleteCard(card) async {
    final prefs = await SharedPreferences.getInstance();
    final token = prefs.getString('token');
    setState(() {
      showSetting = false;
    });
    final response = await post(Uri.parse('${apiEndpoint}card/${card['id']}'),
        headers: {"Authorization": "Bearer $token"});

    if (response.statusCode == 200) {
      await fetchCards();
    }
  }

  linkCard() async {
    setState(() {
      buttonLoading = true;
    });

    final prefs = await SharedPreferences.getInstance();

    final token = prefs.getString('token');
    final response = await post(Uri.parse('${apiEndpoint}card'),
        headers: {"Authorization": "Bearer $token"});

    if (response.statusCode == 200) {
      final body = jsonDecode(response.body);
      setState(() {
        buttonLoading = false;
      });
      pushNewScreen(context,
          screen: LinkCardWebView(
            link: body['xml']['pg_redirect_url'],
          ));
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    getLocale();
    fetchCards();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var w = MediaQuery.of(context).size.width;
    var h = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: AppBar(
        backgroundColor: Colors.white,
        centerTitle: true,
        elevation: 1,
        title: Text(
          translate(language, "cards"),
          style: Theme.of(context).textTheme.headline3,
        ),
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: loading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : Container(
              child: Stack(
                children: [
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 16, horizontal: 10),
                    child: Column(
                      children: [
                        ListView.builder(
                          physics: NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          itemBuilder: (context, index) => GestureDetector(
                            onTap: () {
                              setState(() {
                                card = cards[index];
                                showSetting = true;
                              });
                            },
                            child: Container(
                              margin: EdgeInsets.only(bottom: 16),
                              padding: EdgeInsets.symmetric(
                                  vertical: 15, horizontal: 24),
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(35),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey.withOpacity(0.5),
                                    spreadRadius: 2,
                                    blurRadius: 7,
                                    offset: Offset(
                                        0, 3), // changes position of shadow
                                  ),
                                ],
                              ),
                              child: Row(
                                children: [
                                  Container(
                                    width: 37,
                                    height: 28,
                                    child: Image(
                                      image: AssetImage('assets/card.png'),
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(left: 20),
                                    child: Text(
                                      '${cards[index]['card_hash']}',
                                      style: GoogleFonts.openSans(
                                          fontSize: 16,
                                          fontWeight: FontWeight.w400,
                                          color: Colors.black),
                                    ),
                                  ),
                                  Spacer(),
                                  cards[index]['main'] == 1
                                      ? Container(
                                          width: 25,
                                          height: 25,
                                          margin: EdgeInsets.only(right: 22),
                                          decoration: BoxDecoration(
                                              color: Colors.green,
                                              borderRadius:
                                                  BorderRadius.circular(100)),
                                          child: Icon(
                                            Icons.check,
                                            size: 20,
                                            color: Colors.white,
                                          ),
                                        )
                                      : SizedBox(
                                          height: 20,
                                          width: 20,
                                        ),
                                  Container(
                                    child: Icon(Icons.chevron_right),
                                  )
                                ],
                              ),
                            ),
                          ),
                          itemCount: cards.length,
                        ),
                        GestureDetector(
                          onTap: () {
                            if (!buttonLoading) {
                              linkCard();
                            }
                          },
                          child: Container(
                            padding: EdgeInsets.symmetric(
                                vertical: 15, horizontal: 24),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(35),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.5),
                                  spreadRadius: 2,
                                  blurRadius: 7,
                                  offset: Offset(
                                      0, 3), // changes position of shadow
                                ),
                              ],
                            ),
                            child: buttonLoading
                                ? Center(
                                    child: CircularProgressIndicator(),
                                  )
                                : Row(
                                    children: [
                                      Container(
                                        child: Text(
                                          '${translate(language, "add-card")}',
                                          style: GoogleFonts.openSans(
                                              fontSize: 16,
                                              fontWeight: FontWeight.w400,
                                              color: Colors.black),
                                        ),
                                      ),
                                      Spacer(),
                                      Container(
                                        child: Icon(Icons.chevron_right),
                                      )
                                    ],
                                  ),
                          ),
                        ),
                        Spacer(),
                      ],
                    ),
                  ),
                  showSetting == true
                      ? Positioned(
                          child: SlidingUpPanel(
                          maxHeight: 126,
                          defaultPanelState: PanelState.OPEN,
                          onPanelClosed: () {
                            setState(() {
                              showSetting = false;
                            });
                          },
                          backdropEnabled: true,
                          renderPanelSheet: false,
                          panel: Container(
                            width: w,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.only(
                                    topRight: Radius.circular(5),
                                    topLeft: Radius.circular(5)),
                                boxShadow: [
                                  BoxShadow(
                                    blurRadius: 20.0,
                                    color: Colors.grey,
                                  ),
                                ],
                                color: Colors.white),
                            child: Container(
                              child: Column(
                                children: [
                                  GestureDetector(
                                    onTap: () => chooseCard(card),
                                    child: Container(
                                      padding: EdgeInsets.symmetric(
                                          vertical: 16, horizontal: 32),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            '${translate(language, "main-card")}',
                                            style: GoogleFonts.openSans(
                                                fontWeight: FontWeight.w400,
                                                fontSize: 16,
                                                color: Color(0xff1A1A1A)),
                                          ),
                                          card != null && card['main'] == 1
                                              ? Container(
                                                  width: 30,
                                                  height: 30,
                                                  margin: EdgeInsets.only(
                                                      right: 22),
                                                  decoration: BoxDecoration(
                                                      color: Colors.green,
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              100)),
                                                  child: Icon(
                                                    Icons.check,
                                                    size: 20,
                                                    color: Colors.white,
                                                  ),
                                                )
                                              : SizedBox(),
                                        ],
                                      ),
                                      decoration: BoxDecoration(
                                          border: Border(
                                              bottom: BorderSide(
                                                  color: Color(0xffC4C4C4)))),
                                    ),
                                  ),
                                  GestureDetector(
                                    onTap: () => deleteCard(card),
                                    child: Container(
                                      padding: EdgeInsets.symmetric(
                                          vertical: 16, horizontal: 32),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            translate(language, "delete"),
                                            style: GoogleFonts.openSans(
                                                fontWeight: FontWeight.w400,
                                                fontSize: 16,
                                                color: Color(0xffC92C2C)),
                                          ),
                                        ],
                                      ),
                                      decoration:
                                          BoxDecoration(border: Border()),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                          collapsed: Container(
                            color: Theme.of(context).backgroundColor,
                          ),
                        ))
                      : SizedBox.shrink()
                ],
              ),
            ),
    );
  }
}
