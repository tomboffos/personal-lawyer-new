import 'dart:convert';
import 'dart:io';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart';
import 'package:personal_lawyer_new/views/HomePage.dart';
import 'package:personal_lawyer_new/views/Webview/DocumentWeb.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../language_service.dart';

class UserDocuments extends StatefulWidget {
  const UserDocuments({Key key}) : super(key: key);

  @override
  _UserDocumentsState createState() => _UserDocumentsState();
}

class _UserDocumentsState extends State<UserDocuments> {
  bool loading = false;
  List<dynamic> documents = [];
  Future<String> downloadFile(String url, String fileName) async {
    print(url);
    HttpClient httpClient = new HttpClient();
    File file;
    String filePath = '';
    String myUrl = '';

    try {
      myUrl = url;

      var request = await httpClient.getUrl(Uri.parse(url));
      var response = await request.close();
      if(response.statusCode == 200) {
        var bytes = await consolidateHttpClientResponseBytes(response);
        filePath = 'downloads/personal-lawyer/$fileName';
        file = File(filePath);
        print(file);
        await file.writeAsBytes(bytes);
        print('Good');
      }
      else
        filePath = 'Error code: '+response.statusCode.toString();
    }
    catch(ex){
      filePath = 'Can not fetch url';
    }

    return filePath;
  }
  loadDocuments() async {
    setState(() {
      loading = true;
    });
    final prefs = await SharedPreferences.getInstance();
    final token = prefs.getString('token');
    final response = await get('${apiEndpoint}user/documents',headers:{
      "Authorization" : "Bearer $token"
    });
    print(response.body);
    if (response.statusCode == 200) {
      final body = jsonDecode(response.body);
      print(body);
      setState(() {
        documents = body['data'];
        loading = false;
      });
    }
  }
  String language;

  void getLocale()async{
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      language = prefs.getString('lang');
    });
    print(language);
  }


  @override
  void initState() {
    // TODO: implement initState
    getLocale();
    loadDocuments();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var w = MediaQuery.of(context).size.width;
    var h = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: AppBar(
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
        title: Text(
          translate(language,"documents"),
          style: Theme.of(context).textTheme.headline3,
        ),
        centerTitle: false,
      ),
      body: RefreshIndicator(
        onRefresh: ()=>loadDocuments(),
        child: Container(
            height: h,
            padding: EdgeInsets.only(top: 20, left: 16, right: 16),
            child: loading
                ? Center(
                    child: CircularProgressIndicator(),
                  )
                : ListView.builder(

                    itemBuilder: (context, index) {
                      if(documents.length == 0){
                        return Container(
                          margin: EdgeInsets.only(top: MediaQuery.of(context).size.height/3),
                          child: Center(
                            child: Text('Документов пока нет'),
                          ),
                        );
                      }
                      return Container(
                        margin: EdgeInsets.only(bottom: 5),
                        padding: EdgeInsets.symmetric(
                            vertical: 20, horizontal: 20),
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: Theme.of(context).primaryColor,
                          ),
                          borderRadius: BorderRadius.circular(15),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              width: w - w * 0.3,
                              child: Text(
                                '${documents[index]['document']['name'] }',
                                style: GoogleFonts.openSans(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w400,
                                    color: Theme.of(context).primaryColor),
                              ),
                            ),
                            GestureDetector(
                              onTap: ()async{
                                launch('${documents[index]['document']['document']}');

                                AwesomeDialog(
                                  context: context,
                                  borderSide: BorderSide(color: Colors.green, width: 2),
                                  width: 500,
                                  buttonsBorderRadius: BorderRadius.all(Radius.circular(2)),
                                  headerAnimationLoop: true,
                                  animType: AnimType.BOTTOMSLIDE,
                                  title: 'Успех',
                                  desc: 'Файл успешно загружен',
                                  showCloseIcon: false,
                                  dialogType: DialogType.SUCCES,

                                )..show();

                              },
                              child: Container(
                                width: 42,
                                height: 42,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(100),
                                    color: Color(0xff2A9D8F)),
                                child: Icon(
                                  Icons.download_rounded,
                                  color: Colors.white,
                                ),
                              ),
                            )
                          ],
                        ),
                      );
                    },
                    itemCount: documents.length == 0 ? 1 : documents.length,
                  )),
      ),
    );
  }

  downloadDocument(String link)async {
    final taskId = await FlutterDownloader.enqueue(
      url: link,
      savedDir: '/documents',
      showNotification: true, // show download progress in status bar (for Android)
      openFileFromNotification: true, // click on notification to open downloaded file (for Android)
    );
    print(taskId);
  }
}
