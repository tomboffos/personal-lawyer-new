import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:personal_lawyer_new/views/authorization/Login.dart';
import 'package:personal_lawyer_new/views/authorization/PinCode.dart';
import 'package:personal_lawyer_new/views/chat/Review.dart';
import 'package:personal_lawyer_new/views/profile/Card.dart';
import 'package:personal_lawyer_new/views/profile/ProfileEdit.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../language_service.dart';
import '../HomePage.dart';

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  String language;
  void getLocale()async{
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      language = prefs.getString('lang');
    });
    print(language);
  }
  @override
  void initState() {
    getLocale();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var w = MediaQuery.of(context).size.width;
    var h = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text(
          translate(language,"settings"),
          style: Theme.of(context).textTheme.headline3,
        ),
      ),
      body: Container(
          height: h,
          padding: EdgeInsets.only(top: 20, left: 16, right: 16),
          child: Column(
            children: [
              GestureDetector(
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context)=>ProfileEdit()));
                },
                child: Container(
                  margin: EdgeInsets.only(bottom: 5),
                  padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Theme.of(context).primaryColor,
                    ),
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        width: w - w * 0.3,
                        child: Text(
                          translate(language,"personal-data"),
                          style: GoogleFonts.openSans(
                              fontSize: 16,
                              fontWeight: FontWeight.w400,
                              color: Theme.of(context).primaryColor),
                        ),
                      ),
                      Icon(
                        Icons.chevron_right,
                        color: Theme.of(context).primaryColor,
                      ),
                    ],
                  ),
                ),
              ),
              GestureDetector(
                onTap: (){
                  pushNewScreen(
                    context,
                    screen: PinCodeRegisterPage(isEdit: true,),
                    withNavBar: false, // OPTIONAL VALUE. True by default.
                  );
                },
                child: Container(
                  margin: EdgeInsets.only(bottom: 5),
                  padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Theme.of(context).primaryColor,
                    ),
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        width: w - w * 0.3,
                        child: Text(
                          translate(language,"change-pincode"),
                          style: GoogleFonts.openSans(
                              fontSize: 16,
                              fontWeight: FontWeight.w400,
                              color: Theme.of(context).primaryColor),
                        ),
                      ),
                      Icon(
                        Icons.chevron_right,
                        color: Theme.of(context).primaryColor,
                      ),
                    ],
                  ),
                ),
              ),
              GestureDetector(
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context)=>CardsPage()));
                },
                child: Container(
                  margin: EdgeInsets.only(bottom: 5),
                  padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Theme.of(context).primaryColor,
                    ),
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        width: w - w * 0.3,
                        child: Text(
                          translate(language,"cards"),
                          style: GoogleFonts.openSans(
                              fontSize: 16,
                              fontWeight: FontWeight.w400,
                              color: Theme.of(context).primaryColor),
                        ),
                      ),
                      Icon(
                        Icons.chevron_right,
                        color: Theme.of(context).primaryColor,
                      ),
                    ],
                  ),
                ),
              ),
              // GestureDetector(
              //   onTap: () {
              //     Navigator.push(
              //         context,
              //         MaterialPageRoute(
              //             builder: (context) => Review(
              //                   isSupport: true,
              //                 )));
              //   },
              //   child: Container(
              //     margin: EdgeInsets.only(bottom: 5),
              //     padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
              //     decoration: BoxDecoration(
              //       border: Border.all(
              //         color: Theme.of(context).primaryColor,
              //       ),
              //       borderRadius: BorderRadius.circular(15),
              //     ),
              //     child: Row(
              //       mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //       children: [
              //         Container(
              //           width: w - w * 0.3,
              //           child: Text(
              //             'Тех поддержка',
              //             style: GoogleFonts.openSans(
              //                 fontSize: 16,
              //                 fontWeight: FontWeight.w400,
              //                 color: Theme.of(context).primaryColor),
              //           ),
              //         ),
              //         Icon(
              //           Icons.chevron_right,
              //           color: Theme.of(context).primaryColor,
              //         ),
              //       ],
              //     ),
              //   ),
              // ),
              GestureDetector(
                onTap: () async{
                  final prefs = await SharedPreferences.getInstance();
                  prefs.remove('token');
                  prefs.remove('code');
                  pushNewScreen(context, screen: Login(),withNavBar: false);
                  Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context)=>Login()), (route) => false);

                },
                child: Container(
                  alignment: Alignment.topLeft,
                  margin: EdgeInsets.only(top: 20, left: 10),
                  child: Text(
                    translate(language, "exit"),
                    style: GoogleFonts.openSans(
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                        color: Colors.black),
                    textAlign: TextAlign.start,
                  ),
                ),
              )
            ],
          )),
    );
  }
}
