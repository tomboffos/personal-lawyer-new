import 'dart:convert';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart';
import 'package:personal_lawyer_new/language_service.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import '../HomePage.dart';

class ProfileEdit extends StatefulWidget {
  @override
  _ProfileEditState createState() => _ProfileEditState();
}

class _ProfileEditState extends State<ProfileEdit> {
  bool hideAvatarSettings = true;
  PanelController mainController = PanelController();
  Map<String,dynamic> user;
  TextEditingController name = new TextEditingController();
  TextEditingController phone = new MaskedTextController(mask: '+0 (000) 000-00-00', text: '+7');
  TextEditingController surname = new TextEditingController();
  String language;
  void getLocale()async{
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      language = prefs.getString('lang');
    });
  }

  getUserData() async {
    final prefs = await SharedPreferences.getInstance();
    final token = prefs.getString('token');
    final response = await get(Uri.parse('${apiEndpoint}user'),
        headers: {"Authorization": "Bearer $token"});

    if(response.statusCode == 200){
      final body = jsonDecode(response.body);
      setState(() {
        user = body['data'];
        name.text = user['name'];
        phone.text = user['phone'];
        surname.text = user['surname'];
      });
    }
  }

  updateProfile()async{
    final prefs = await SharedPreferences.getInstance();
    final token = prefs.getString('token');
    final response = await post('${apiEndpoint}user',headers:{
      "Authorization" : "Bearer $token",
      "Accept" : 'application/json'
    },body:{
      "name" : name.text,
      "phone" : phone.text,
      "surname" : surname.text
    });
    final body = jsonDecode(response.body);
    print(body);
    if(response.statusCode == 200){
      await getUserData();
      AwesomeDialog(
        context: context,
        borderSide: BorderSide(color: Colors.green, width: 2),
        width: 500,
        buttonsBorderRadius: BorderRadius.all(Radius.circular(2)),
        headerAnimationLoop: true,
        animType: AnimType.BOTTOMSLIDE,
        title: 'Успешно',
        desc: 'Ваш профиль успешно сохранен',
        showCloseIcon: false,
        dialogType: DialogType.SUCCES,

      )..show();
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    getLocale();
    getUserData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var w = MediaQuery.of(context).size.width;
    var h = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text(
          translate(language, 'personal-data'),
          style: Theme.of(context).textTheme.headline3,
        ),
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: user == null ? Center(
        child: CircularProgressIndicator(),
      ) : Stack(
        children: [
          Container(
            width: w,
            padding: EdgeInsets.symmetric(horizontal: 16, vertical: 28),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                GestureDetector(
                  onTap: () {
                    setState(() {
                      hideAvatarSettings = true;
                    });
                  },
                  child: Container(
                    height: 90,
                    width: 90,
                    decoration:
                        BoxDecoration(borderRadius: BorderRadius.circular(100)),
                    child: Image(
                      image: NetworkImage(user['avatar']),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                Container(
                  margin:EdgeInsets.only(top:20),
                  child: Center(
                    child: Text('${translate(language,'balance')}: ${user['balance']} тг'),
                  ),
                ),
                Container(
                  width: w - 32,
                  margin: EdgeInsets.only(top: 44),
                  child: TextField(
                    controller: name,
                    decoration: InputDecoration(
                        labelText: '${translate(language,'name')}',
                        floatingLabelBehavior: FloatingLabelBehavior.always,
                        labelStyle: TextStyle(
                            color: Theme.of(context).primaryColor, fontSize: 20),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(50),
                          borderSide: BorderSide(color: Theme.of(context).primaryColor),
                        ),
                        border: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(100),
                          borderSide: new BorderSide(
                              color: Theme.of(context).primaryColor),
                        )),


                  ),
                ),
                Container(
                  width: w - 32,
                  margin: EdgeInsets.only(top: 44),
                  child: TextField(
                    controller: surname,
                    decoration: InputDecoration(
                        labelText: '${translate(language,'surname')}',
                        floatingLabelBehavior: FloatingLabelBehavior.always,
                        labelStyle: TextStyle(
                            color: Theme.of(context).primaryColor, fontSize: 20),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(50),
                          borderSide: BorderSide(color: Theme.of(context).primaryColor),
                        ),
                        border: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(100),
                          borderSide: new BorderSide(
                              color: Theme.of(context).primaryColor),
                        )),
                  ),
                ),
                Container(
                  width: w - 32,
                  margin: EdgeInsets.only(top: 44),
                  child: TextField(
                    controller: phone,
                    decoration: InputDecoration(
                        labelText: '${translate(language,'phone')}',
                        floatingLabelBehavior: FloatingLabelBehavior.always,

                        labelStyle: TextStyle(
                            color: Theme.of(context).primaryColor, fontSize: 20),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(50),
                          borderSide: BorderSide(color: Theme.of(context).primaryColor),
                        ),
                        border: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(100),
                          borderSide: new BorderSide(
                              color: Theme.of(context).primaryColor),
                        )),
                  ),
                ),
                GestureDetector(
                  onTap: ()=>updateProfile(),
                  child: Container(
                    width: w - 32,
                    margin: EdgeInsets.only(top: 40),
                    child: Container(
                      padding: EdgeInsets.symmetric(vertical: 15),
                      decoration: BoxDecoration(
                          color: Theme.of(context).primaryColor,
                          borderRadius: BorderRadius.circular(25)),
                      child: Text(
                        '${translate(language,'save-data')}',
                        style: GoogleFonts.openSans(
                          fontSize: 16,
                          fontWeight: FontWeight.w600,
                          color: Colors.white,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
          hideAvatarSettings == false
              ? Positioned(
                  child: SlidingUpPanel(
                  maxHeight: 220,
                  defaultPanelState: PanelState.OPEN,
                  onPanelClosed: () {
                    setState(() {
                      hideAvatarSettings = true;
                    });
                  },
                  controller: mainController,
                  backdropEnabled: true,
                  renderPanelSheet: false,
                  panel: Container(
                    width: w,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topRight: Radius.circular(5),
                            topLeft: Radius.circular(5)),
                        boxShadow: [
                          BoxShadow(
                            blurRadius: 20.0,
                            color: Colors.grey,
                          ),
                        ],
                        color: Colors.white),
                    child: Container(
                      child: Column(
                        children: [
                          Container(
                            padding: EdgeInsets.symmetric(
                                vertical: 16, horizontal: 32),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  'Сделать снимок',
                                  style: GoogleFonts.openSans(
                                      fontWeight: FontWeight.w400,
                                      fontSize: 16,
                                      color: Color(0xff1A1A1A)),
                                ),
                              ],
                            ),
                            decoration: BoxDecoration(
                                border: Border(
                                    bottom:
                                        BorderSide(color: Color(0xffC4C4C4)))),
                          ),
                          Container(
                            padding: EdgeInsets.symmetric(
                                vertical: 16, horizontal: 32),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  'Добавить фото',
                                  style: GoogleFonts.openSans(
                                      fontWeight: FontWeight.w400,
                                      fontSize: 16,
                                      color: Color(0xff1A1A1A)),
                                ),
                              ],
                            ),
                            decoration: BoxDecoration(
                                border: Border(
                                    bottom:
                                        BorderSide(color: Color(0xffC4C4C4)))),
                          ),
                          Container(
                            padding: EdgeInsets.symmetric(
                                vertical: 16, horizontal: 32),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  'Удалить',
                                  style: GoogleFonts.openSans(
                                      fontWeight: FontWeight.w400,
                                      fontSize: 16,
                                      color: Color(0xffC92C2C)),
                                ),
                              ],
                            ),
                            decoration: BoxDecoration(
                                border: Border(
                                    bottom:
                                        BorderSide(color: Color(0xffC4C4C4)))),
                          ),
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                hideAvatarSettings = true;
                              });
                            },
                            child: Container(
                              padding: EdgeInsets.symmetric(
                                  vertical: 16, horizontal: 32),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    'Отмена',
                                    style: GoogleFonts.openSans(
                                        fontWeight: FontWeight.w400,
                                        fontSize: 16,
                                        color: Color(0xff1A1A1A)),
                                  ),
                                ],
                              ),
                              decoration: BoxDecoration(
                                  border: Border(
                                      bottom: BorderSide(
                                          color: Color(0xffC4C4C4)))),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  collapsed: Container(
                    color: Theme.of(context).backgroundColor,
                  ),
                ))
              : SizedBox.shrink()
        ],
      ),
    );
  }
}
